<div class="topbar">
        <!-- Navbar -->
        <nav class="navbar-custom">
        <ul class="list-unstyled topbar-nav mb-0">
    <li><a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button"
                        aria-haspopup="false" aria-expanded="false"><span src="<?= base_url('assets/images/logo-sm.png') ?>"
                            alt="profile-user" class="rounded-circle"></span> </a></li>
    <li><button class="button-menu-mobile nav-link waves-effect waves-light"><svg xmlns="http://www.w3.org/2000/svg"
                width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu align-self-center">
                <line x1="3" y1="12" x2="21" y2="12"></line>
                <line x1="3" y1="6" x2="21" y2="6"></line>
                <line x1="3" y1="18" x2="21" y2="18"></line>
            </svg></button>
    </li>
</ul>

            <ul class="list-unstyled topbar-nav float-right mb-0">
                <li class="dropdown notification-list">
                    <div class="dropdown-menu dropdown-menu-right dropdown-lg py-0">
                </li>

                <li class="dropdown">
                    <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button"
                        aria-haspopup="false" aria-expanded="false"><img src="<?= base_url('assets/images/users/user-1.jpg') ?>"
                            alt="profile-user" class="rounded-circle"> <span class="ml-1 nav-user-name hidden-sm"><?=$this->session->userdata('nama_role')." ".$this->session->userdata('username')?>
                            <i class="mdi mdi-chevron-down"></i></span></a>
                    <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#"><i
                                class="dripicons-user text-muted mr-2"></i> Setting</a>
                        <div class="dropdown-divider"></div><a class="dropdown-item bg-light" href="<?= base_url('login/logout_process') ?>"><i
                                class="dripicons-exit text-muted mr-2"></i> Logout</a>
                    </div>
                </li>
            </ul>
            <!--end topbar-nav-->
        </nav><!-- end navbar-->
    </div><!-- Top Bar End -->