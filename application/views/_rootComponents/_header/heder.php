<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from mannatthemes.com/metrica/metrica_simple/ecommerce/ecommerce-index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 13 Oct 2020 22:35:19 GMT -->

<head>
    <meta charset="utf-8">
    <title>Biotracking</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description">
    <meta content="" name="author">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App favicon -->    
    <link rel="shortcut icon" href=<?= base_url('assets/images/logo-sm.png')?>
    <link href="<?= base_url('assets/plugins/lightpick/lightpick.css') ?>" rel="stylesheet"><!-- App css -->
    <link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/css/jquery-ui.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/icons.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/css/metisMenu.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/css/app.css') ?>" rel="stylesheet" type="text/css">
    <script src="<?= base_url('assets/js/jquery.min.js')?>"></script>
    <script src="<?= base_url('assets/js/jquery-ui.min.js')?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.bundle.min.js')?>"></script>
    <link href="<?= base_url('assets/plugins/sweetAlert/sweetAlert.css') ?>" rel="stylesheet" type="text/css">
    <script src="<?= base_url('assets/plugins/sweetAlert/sweetAlert.js') ?>"></script>
    <script>
        function showLoader(){
            document.querySelector("body").classList.add("myStyle");
            document.querySelector(".load").classList.remove("hidden");
        }

        function hideLoader(){
            document.querySelector(".load").classList.add("hidden");
            document.querySelector("body").classList.remove("myStyle");
        }  
    </script>
    <style type="text/css">
        .myStyle{
            filter: blur(3px);
            -webkit-filter: blur(3px);
        }
        body{ 
            width:100%;
            height:100%;
            margin:0;
            background-color:#fff;
        }
        .load{
            z-index: 999;
            position:absolute;
            max-width:100px;
            margin:0 auto;
            top: 50%;
            left:50%;
            transform: translate(-50%, -50%);
        }
        /*loading screen*/
        .loading-screen{
            float:left;
            height:20px;
            width: 20px;
            margin:0 5px;
            border-radius:50%;
            animation: shrink 1s ease infinite 0ms;
            transform: scale(0.35);
        }
        /* animation */
        .loading-screen:nth-child(1){
            animation: shrink 1s ease infinite 350ms;
            background-color:#45aaf2;
        }

        .loading-screen:nth-child(2){
            animation: shrink 1s ease infinite 550ms;
            background-color:#ffb8b8;
        }

        .loading-screen:nth-child(3){
            animation: shrink 1s ease infinite 700ms; 
            background-color:#f9ca24;
        }
        
        @keyframes shrink{
            50%{
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 1;
            }
  
            100%{
                opacity: 0;
            }
        }

        .hidden{
            display: none;
        }
    </style>
</head>
<body>
<script>
    showLoader(); 
</script>
<div class="load">
  <div class="loading-screen"></div>
  <div class="loading-screen"></div>
  <div class="loading-screen"></div>
</div>