    <script src="<?= base_url('assets/js/metismenu.min.js')?>"></script>
    <script src="<?= base_url('assets/js/waves.js')?>"></script>
    <script src="<?= base_url('assets/js/feather.min.js')?>"></script>
    <script src="<?= base_url('assets/js/jquery.slimscroll.min.js')?>"></script>
    <script src="<?= base_url('assets/plugins/apexcharts/apexcharts.min.js')?>"></script>
    <script src="<?= base_url('assets/plugins/chartjs/chart.min.js')?>"></script>
    <script src="<?= base_url('assets/plugins/chartjs/roundedBar.min.js')?>"></script>
    <script src="<?= base_url('assets/js/app.js')?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>

    <script>
        $(document).ready(function(){
            hideLoader(); 
        });  
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function () {
            'use strict';
            window.addEventListener('load', function () {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function (form) {
                    form.addEventListener('submit', function (event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>

    </body>
    <!-- Mirrored from mannatthemes.com/metrica/metrica_simple/ecommerce/ecommerce-index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 13 Oct 2020 22:35:31 GMT -->

    </html>