<body class="light-sidenav">
    <div class="left-sidenav" class="dark-sidenav">
        <!-- LOGO -->
        <div class="topbar-left" style="color: while !important;"><a href="<?= base_url('home') ?>" class="logo">
                <span style="color: white !important; font-size: 20px;">
                    <img src="<?= base_url('assets/images/logo-sm.png') ?>" alt="logo-small" class="logo-sm" width="50"
                        style="height: 50px;">
                </span></a>
        </div>

        <!--end logo-->
        <div class="leftbar-profile p-3 w-100">
            <div class="media position-relative">
                <div class="media-body align-self-center text-truncate">
                    <h5 class="mt-0 mb-1 font-weight-semiboldk" align="center">
                        <?=$this->session->userdata('nm_role_lv_2');?></h5>
                </div>
                <!--end media-body-->
            </div>
        </div>

        <ul class="metismenu left-sidenav-menu slimscroll">
            <?php if(has_permission_menu('Dashboard') > 0) { ?>
            <li id="PageDashboard" class="leftbar-menu-item"><a href="javascript: void(0);" class="menu-link"><i
                        data-feather="aperture" class="align-self-center vertical-menu-icon icon-dual-vertical"></i>
                    <span>Dashboard</span>
                    <span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li class="nav-item"><a class="nav-link" href="<?= base_url('home'); ?>"><i
                                class="ti-control-record"></i>Dashboard</a></li>
                </ul>
            </li>
            <?php } ?>
            <?php if(has_permission_menu('Menu') > 0) { ?>
            <li id="PagePermission" class="leftbar-menu-item"><a href="javascript: void(0);" class="menu-link"><i data-feather="key"
                        class="align-self-center vertical-menu-icon icon-dual-vertical"></i> <span>Permission</span> <span
                        class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li><a href="javascript: void(0);"><i class="ti-control-record"></i>Menu <span
                                class="menu-arrow left-has-menu"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li>
                                <a href="<?= base_url('menu/list') ?>">
                                    List Menu
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('menu/create') ?>">
                                    Create Menu
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="javascript: void(0);"><i class="ti-control-record"></i>Permission Web <span
                                class="menu-arrow left-has-menu"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li><a href="<?= base_url('permission/listPermissionWeb') ?>">
                                    List Permission Web
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('permission/createPermissionWeb') ?>">
                                    Create Permission Web
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="javascript: void(0);"><i class="ti-control-record"></i>Permission Mobile <span
                                class="menu-arrow left-has-menu"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li>
                                <a href="<?= base_url('permission/listPermissionMobile') ?>">
                                    List Permission Mobile
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('permission/createPermissionMobile') ?>">
                                    Create Permission Mobile
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <?php }?>
            <?php if(has_permission_menu('Emergency') > 0) { ?>
            <li id="PageEmergency" class="leftbar-menu-item"><a href="javascript: void(0);" class="menu-link"><i
                        data-feather="alert-triangle" class="align-self-center vertical-menu-icon icon-dual-vertical"></i>
                    <span>Emergency</span>
                    <span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li class="nav-item"><a class="nav-link" href="<?= base_url('emergency/list') ?>"><i
                                class="ti-control-record"></i>Master Emergency</a></li>
                </ul>
            </li>
            <?php }?>
            <?php if(has_permission_menu('Recooling') > 0) { ?>
            <li id="PageRecooling" class="leftbar-menu-item"><a href="javascript: void(0);" class="menu-link"><i
                        data-feather="clipboard" class="align-self-center vertical-menu-icon icon-dual-vertical"></i>
                    <span>Recolling</span>
                    <span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li class="nav-item"><a class="nav-link" href="<?= base_url('recooling/list') ?>"><i
                                class="ti-control-record"></i>Master Recolling</a></li>
                </ul>
            </li>
            <?php }?>
            <?php if(has_permission_menu('User') > 0) { ?>
            <li id="PageUser" class="leftbar-menu-item"><a href="javascript: void(0);" class="menu-link"><i data-feather="users"
                        class="align-self-center vertical-menu-icon icon-dual-vertical"></i> <span>Role</span> <span
                        class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li><a href="javascript: void(0);"><i class="ti-control-record"></i>User Role <span
                                class="menu-arrow left-has-menu"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li>
                                <a href="<?= base_url('role/list') ?>">
                                    List User Role
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('role/create') ?>">
                                    Create User Role
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="javascript: void(0);"><i class="ti-control-record"></i>User Role 2 <span
                                class="menu-arrow left-has-menu"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li><a href="<?= base_url('role/list_lvl_2') ?>">
                                    List Instansi/Warehouse
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('/role/create_lvl_2') ?>">
                                    Create Instansi/Warehouse
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="javascript: void(0);"><i class="ti-control-record"></i>User Account <span
                                class="menu-arrow left-has-menu"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li>
                            <a href="<?= base_url('user/user_account/list') ?>">List User Account</a>
                            </li>
                            <li>
                            <a href="<?= base_url('user/user_account/create') ?>">Create User Account</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <?php } ?>
            <?php if(has_permission_menu('Delivery Order') > 0) { ?>
            <!-- <li id="manajemenApi" class="leftbar-menu-item"><a href="javascript: void(0);" class="menu-link"><i data-feather="package"
                        class="align-self-center vertical-menu-icon icon-dual-vertical"></i> <span>Delivery Order</span> <span
                        class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li class="nav-item"><a class="nav-link" href="<?= base_url('delivery_order/create'); ?>"><i
                                class="ti-control-record"></i>Create Order</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?= base_url('delivery_order/list'); ?>"><i
                                class="ti-control-record"></i>List Order</a></li>
                        </ul>
            </li> -->
            <?php }?>
            <?php if(has_permission_menu('Retur') > 0) { ?>
            <li id="manajemenApi" class="leftbar-menu-item"><a href="javascript: void(0);" class="menu-link"><i data-feather="corner-up-left"
                        class="align-self-center vertical-menu-icon icon-dual-vertical"></i> <span>Retur Order</span> <span
                        class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li class="nav-item"><a class="nav-link" href="<?= base_url('retur/create'); ?>"><i
                                class="ti-control-record"></i>Create Retur</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?= base_url('retur/list'); ?>"><i
                                class="ti-control-record"></i>List Retur</a></li>
                        </ul>
            </li>
            <?php }?>
            <?php if(has_permission_menu('Receive') > 0) { ?>
            <li id="manajemenApi" class="leftbar-menu-item"><a href="javascript: void(0);" class="menu-link"><i data-feather="box"
                        class="align-self-center vertical-menu-icon icon-dual-vertical"></i> <span>Receive</span> <span
                        class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li class="nav-item"><a class="nav-link" href="<?= base_url('receive_order/list'); ?>"><i
                                class="ti-control-record"></i>List Receive</a></li>
                        </ul>
            </li>
            <?php }?>
            <?php if(has_permission_menu('Inventory') > 0) { ?>
            <li id="manajemenApi" class="leftbar-menu-item"><a href="javascript: void(0);" class="menu-link"><i data-feather="book"
                        class="align-self-center vertical-menu-icon icon-dual-vertical"></i> <span>Inventory</span> <span
                        class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li class="nav-item"><a class="nav-link" href="<?= base_url('inventory/list'); ?>"><i
                                class="ti-control-record"></i>List Inventory</a></li>
                        </ul>
            </li>
            <?php }?>
            <?php if(has_permission_menu('Manajemen Api') > 0) { ?>
            <li id="manajemenApi" class="leftbar-menu-item"><a href="javascript: void(0);" class="menu-link"><i data-feather="code"
                        class="align-self-center vertical-menu-icon icon-dual-vertical"></i> <span>Manajemen Open Api</span> <span
                        class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li><a href="javascript: void(0);"><i class="ti-control-record"></i>Partisipan<span
                                class="menu-arrow left-has-menu"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li>
                                <a href="<?= base_url('manajemen_open_api/Partisipan/list')?>">
                                    List Partisipan
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('manajemen_open_api/Partisipan/create')?>">
                                    Create Partisipan
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="javascript: void(0);"><i class="ti-control-record"></i>URL <span
                                class="menu-arrow left-has-menu"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li><a href="<?= base_url('manajemen_open_api/Url/list')?>">
                                    List URL
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="javascript: void(0);"><i class="ti-control-record"></i>Akses API<span
                                class="menu-arrow left-has-menu"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li>
                            <a href="<?= base_url('manajemen_open_api/Akses_api/list')?>">List Akses</a>
                            </li>
                            <li>
                            <a href="<?= base_url('manajemen_open_api/Akses_api/create')?>">Create Akses</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <?php }?>

            <?php if($this->session->userdata('nm_role_lv_2') == "Administrator") { ?>
            <li id="reportData" class="leftbar-menu-item"><a href="javascript: void(0);" class="menu-link"><i
                        data-feather="file-text" class="align-self-center vertical-menu-icon icon-dual-vertical"></i>
                    <span>Report Data</span>
                    <span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('emergency/report')?>"><i
                                class="ti-control-record"></i>Report Emergency
                        </a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('delivery_order/trackBackByGsOne')?>"><i
                                class="ti-control-record"></i>Track GsOneId
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('delivery_order/trackBackByNoDo')?>"><i
                                class="ti-control-record"></i>Track Order
                        </a>
                    </li> -->
                    <li class="nav-item"><a class="nav-link" href="<?= base_url('delivery_order/list'); ?>"><i
                                class="ti-control-record"></i>List Order</a></li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('log/log_admin')?>"><i
                                class="ti-control-record"></i>Data Log
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('log/log_visibility')?>"><i
                                class="ti-control-record"></i>Activity
                        </a>
                    </li>
                </ul>
            </li>
            <?php }?>

            <?php if(has_permission_menu('Report') > 0) { ?>
            <li id="reportData" class="leftbar-menu-item"><a href="javascript: void(0);" class="menu-link"><i
                        data-feather="file-text" class="align-self-center vertical-menu-icon icon-dual-vertical"></i>
                    <span>Report Data</span>
                    <span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li class="nav-item"><a class="nav-link" href="<?= base_url('delivery_order/list'); ?>"><i
                                class="ti-control-record"></i>List Order</a></li>
                </ul>
            </li>
            <?php }?>

            <?php if(has_permission_menu('Scan Delivery') > 0) { ?>
            <li id="reportData" class="leftbar-menu-item"><a href="javascript: void(0);" class="menu-link"><i
                        data-feather="share" class="align-self-center vertical-menu-icon icon-dual-vertical"></i>
                    <span>Scan Delivery</span>
                    <span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li class="nav-item"><a class="nav-link" href="<?= base_url('delivery_order/scanDelivery'); ?>"><i
                                class="ti-control-record"></i>Scan</a></li>
                </ul>
            </li>
            <?php }?>
        </ul>
    </div><!-- end left-sidenav-->
</body>