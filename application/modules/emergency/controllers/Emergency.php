<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Emergency extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 		$this->load->model('Emergency_model');
 		$this->load->model('delivery_order/Delivery_order_model');
 		if(!$this->session->has_userdata('nama')){
 			redirect(base_url("login"));
 		}
 	}

 	public function createEmergency()
 	{
 		$data = [
					   
 		];
 		$input_data = $this->Emergency_model->c_emergency($data);
 		if ($this->input->post('emergency_do') != null) {
 			$this->createDetail($this->input->post('emergency_do'));
 		}
 		if($input_data){
 			echo json_encode(['message' => 'input data berhasil']);
 		}else{
 			echo json_encode(['message' => 'input data gagal']);
 		}
 	}

 	public function createEmergencyDo($emergency_do){
 		foreach ($emergency_do as $data) {
 			$data = [
 				"id_emergency" => $data['id_emergency'],
 				"no_do" => $data['no_do'],
 			];
 			$input_data = $this->Emergency_model->c_emergency_do($data);
 			$input_data = $this->Delivery_order_model->u_status_emergency_delivery_order($data['no_do']);
 		}
 		if($input_data){
 			return true;
 		}else{
 			return false;
 		}
 	}

 	public function getEmergency()
 	{
 		$startDate = $this->input->post('startDate');
 		$endDate = $this->input->post('endDate');
		$list = $this->Emergency_model->r_emergency($startDate, $endDate);
		$all_data = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$row = array();
			$no++;
			$row[] = $no;
			$row[] = $data->category;
			$row[] = $data->description;
			$row[] = $data->foto;
			$row[] = $data->new_no_pol;
			$row[] = $data->time_emergency;
			$row[] = $data->username;
			$row[] = '<a href="'.base_url('emergency/listDoEmergency/').$data->id_emergency.'" class="">
					   	<i class="las la-clipboard text-info font-18"></i>
					   </a> ';
			$all_data[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->Emergency_model->r_emergency_count_all(),
                        "recordsFiltered" => $this->Emergency_model->r_emergency_count_filtered($startDate, $endDate),
                        "data" => $all_data,
		); 
 		echo json_encode($result);
 	}

 	public function getEmergency2()
 	{
 		$startDate = $this->input->post('startDate');
 		$endDate = $this->input->post('endDate');
		$list = $this->Emergency_model->r_emergency($startDate, $endDate);
		$all_data = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$row = array();
			$no++;
			$row[] = $no;
			$row[] = $data->username;
			$row[] = $data->category;
			$row[] = $data->time_emergency;
			$row[] = $data->description;
			$all_data[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->Emergency_model->r_emergency_count_all(),
                        "recordsFiltered" => $this->Emergency_model->r_emergency_count_filtered($startDate, $endDate),
                        "data" => $all_data,
		); 
 		echo json_encode($result);
 	}

 	public function getReportEmergency()
 	{
 		$id = $this->input->post('id_emergency');
		$list = $this->Emergency_model->r_report_emergency($id);
		$all_data = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$row = array();
			$no++;
			$row[] = $no;
			$row[] = $data->no_do;
			$row[] = $data->eta;
			$row[] = $data->no_request;
			$row[] = $data->nm_role_lvl_2;
			$all_data[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->Emergency_model->r_report_emergency_count_all($id),
                        "recordsFiltered" => $this->Emergency_model->r_report_emergency_count_filtered(),
                        "data" => $all_data,
		); 
 		echo json_encode($result);
 	}

 	public function exportReportEmergency(){
 		$data = $this->Emergency_model->r_report_emergency_export();
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'No');
		$sheet->setCellValue('B1', 'Kategori Recooling');
		$sheet->setCellValue('C1', 'Description');
		$sheet->setCellValue('D1', 'Nomor Polisi');
		$sheet->setCellValue('E1', 'Time Reccoling');
		$sheet->setCellValue('F1', 'User');
		$no = 1;
		$x = 2;
		foreach($data as $row)
		{
			$sheet->setCellValue('A'.$x, $no++);
			$sheet->setCellValue('B'.$x, $row->category);
			$sheet->setCellValue('C'.$x, $row->description);
			$sheet->setCellValue('D'.$x, $row->new_no_pol);
			$sheet->setCellValue('E'.$x, $row->time_emergency);
			$sheet->setCellValue('F'.$x, $row->username);
			$x++;
		}
		$writer = new Xlsx($spreadsheet);
		$filename = 'Report_emergency';
			
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');
	
		$writer->save('php://output');
 	}
	public function getEmergencyById($id){
		echo json_encode($this->Emergency_model->r_emergency_by_id($id));
	}
	 
	// FOR VIEW
	 public function list()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('list_emergency');
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function listDoEmergency($id)
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('list_do_emergency', ['id' => $id]);
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function detail($id)
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('detail_emergency', ['id' => $id]);
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function create()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('create_emergency');
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function update($id)
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('update_emergency', ['id' => $id]);
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function report()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('report_emergency');
		$this->load->view('_rootComponents/_footer/footer');
	 }
}