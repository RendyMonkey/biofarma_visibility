<?php 
class Emergency_model extends CI_Model{

	public function c_emergency($data){
		$this->db->insert('emergency', $data);
		return $this->db->insert_id();
	}

	public function c_emergency_do($data){
		return $this->db->insert('emergency_do', $data);
	}

	public function r_emergency_query($startDate = "", $endDate = ""){

		$search_attribut = ["emergency.id_emergency", "emergency.category", "emergency.description", "emergency.foto", "emergency.new_no_pol", "emergency.time_emergency"," user_account.username"];
		$column_order = array(null, "emergency.id_emergency", "emergency.category", "emergency.description", "emergency.foto", "emergency.new_no_pol", "emergency.time_emergency"," user_account.username");
		$this->db->select("emergency.id_emergency, emergency.category, emergency.description, emergency.foto, emergency.new_no_pol, emergency.time_emergency, user_account.username");
		$this->db->from('emergency');
		$this->db->join('user_account', 'user_account.id_user_account = emergency.id_user_account');
		if ($startDate != "" AND $endDate != "") {
			$this->db->where('emergency.created_date >=', $startDate);
			$this->db->where('emergency.created_date <=', $endDate);
		}else if($startDate != "" AND $endDate == ""){
			$this->db->where('emergency.created_date >=', $startDate);
		}else if($startDate == "" AND $endDate != ""){
			$this->db->where('emergency.created_date <=', $endDate);
		}
		$i = 0;	
	}

	public function r_report_emergency_query($id){
		$search_attribut = ["delivery_order.no_do", "delivery_order.eta", "delivery_order.no_request", "user_role_level_2.nm_role_lvl_2"];
		$column_order = array(null, "delivery_order.no_do", "delivery_order.eta", "delivery_order.no_request", "user_role_level_2.nm_role_lvl_2");
		$this->db->select("delivery_order.no_do, delivery_order.eta, delivery_order.no_request, user_role_level_2.nm_role_lvl_2");
		$this->db->from('emergency');
		$this->db->join('emergency_do', 'emergency_do.id_emergency = emergency.id_emergency');
		$this->db->join('delivery_order', 'delivery_order.no_do = emergency_do.no_do');
		$this->db->join('user_role_level_2', 'user_role_level_2.id_user_role = delivery_order.id_role_receive');
		$this->db->where('emergency.id_emergency', $id);
		$i = 0;
	}

	public function r_emergency($startDate, $endDate){
		$this->r_emergency_query($startDate, $endDate);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	public function r_report_emergency($id){
		$this->r_report_emergency_query($id);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	public function r_report_emergency_export(){
		$this->r_emergency_query();
		$query = $this->db->get();
		return $query->result();
	}

	public function u_emergency($id, $data){
		$this->db->where('id_emergency', $id);
		$query = $this->db->update('emergency', $data);
		return $query;
	}

	public function d_emergency($id){
		$query = $this->db->delete('emergency', array('id_emergency' => $id));
		return $query;
	}

	public function d_detail_emergency($id){
		$query = $this->db->delete('detail_emergency', array('id_emergency' => $id));
		return $query;
	}

	public function r_emergency_count_all()
    {
        $this->db->from('emergency');
        return $this->db->count_all_results();
	}

	public function r_report_emergency_count_all($id)
    {
        $this->db->from('emergency');
        $this->db->get_where('id_emergency', $id);
        return $this->db->count_all_results();
	}
	
	function r_emergency_count_filtered($startDate, $endDate)
    {
        $this->r_emergency_query($startDate, $endDate);
        $query = $this->db->get();
        return $query->num_rows();
	}

	function r_report_emergency_count_filtered()
    {
        $this->r_report_emergency_query();
        $query = $this->db->get();
        return $query->num_rows();
	}

	function r_emergency_by_id($id){
		$this->db->select("emergency.id_emergency, emergency.category, emergency.description, emergency.new_no_pol, emergency.time_emergency, user_account.username");
		$this->db->from('emergency');
		$this->db->join('user_account', 'user_account.id_user_account = emergency.id_user_account');
		$query = $this->db->where('emergency.id_emergency', $id)->get()->row();
		return $query;
	}
}
?>