<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url() ?>">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">Emergency</a></li>
                                    <li class="breadcrumb-item active">Detail Emergency</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Detail Emergency</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <img id="photo" src="" class="img-fluid">
                                </div>
                                    <div class="col-lg-6 align-self-center">
                                        <div class="single-pro-detail">
                                            <p class="mb-1">Detail Emergency</p>
                                            <div class="custom-border mb-3"></div>
                                            <h3 class="pro-title" id="kategori_emergency">Low stock of medicine COVID-19 on other Hospital</h3>
                                            <p class="text-muted mb-0" id="no_emergency"></p>
                                           
                                           <label>Emergency Time</label>
                                            <h6 class="pro-price" id="timeEmergency">Wednesday, 15 October 2020</h6>
                                            <h6 class="text-muted font-13">Description :</h6>
                                            <ul class="list-unstyled pro-features border-0" id="description">
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
                                            </ul>
                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- container -->
            <!--end footer-->
        </div><!-- end page content -->
    </div><!-- end page-wrapper -->

    <script>
      $.ajax({
        url: "<?= base_url('emergency/getEmergencyWithDetailById/').$id ?>",
        type: 'get',
        dataType: 'json',
        success: function (data) {
            $("#kategori_emergency").text(data.kategori_emergency);
            $("#no_emergency").text('#' + data.no_emergency);
            $("#timeEmergency").text(data.timeEmergency);
            $("#description").text(data.description);
            $("#photo").attr('src', '<?= base_url('assets/images/emergency/') ?>' + data.foto);

        }
    });
    </script>