<link href="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/moment/moment.js') ?>"></script>
<script src="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.js') ?>"></script>
<link href="<?= base_url('assets/plugins/dropify/dropify.min.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/dropify/dropify.min.js') ?>"></script>

<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                
                            <li class="breadcrumb-item"><a href="<?= base_url() ?>">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">Emergency</a></li>
                                    <li class="breadcrumb-item active">Create Emergency</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Create Emergency</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form class=" col-8 offset-2" enctype="multipart/form-data" id="frm">
                                <div class="form-group">
                                    <label for="category">Emergency Category</label>
                                    <input type="text" class="form-control" name="kategori_emergency" id="category">
                                </div>
                                <div class="form-group">
                                    <label for="shipment">Shipment</label>
                                    <select class="form-control" name="id_shipment" id="shipment">
                                        <option>Choose...</option>
                                        <option value="1">1</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="shipment">User Account</label>
                                    <select class="form-control" name="id_user_account" id="id_user_account">
                                        <option>Choose...</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="time">Time Emergency</label>
                                    <input type="text" id="date-format" name="timeEmergency" class="form-control"
                                        placeholder="Saturday 24 June 2017 - 21:44">
                                </div>

                                <div class="form-group">
                                    <label for="category">Emergency Description</label>
                                    <textarea class="form-control" name="description" id="description" rows="4"></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="photo">Photo</label>
                                    <input type="file" name="foto_emergency" class="dropify" id="foto"  data-show-remove="false" data-height="300">
                                </div>

                                <button type="button" id="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container -->
    </div><!-- end page content -->
</div><!-- end page-wrapper -->

<script>
    $('#date-format').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY',
        time: false,
    });
    $('.dropify').dropify();

    $.ajax({
            url: '<?= base_url('/user/User_account/getUserAccountWithoutDatatables') ?>',
            type: 'get',
            dataType: 'json',
            success: function(data){
                console.log(data);
                var html = '';
                $.each(data, function(key, dataValue){
                    html = '<option value="' + dataValue.id_user_account + '">' + dataValue.nm_lengkap + '</option>'
                    $("#id_user_account").append(html);
                });
            }
        });

    $(document).ready(function () {
        $("#submit").click(function () {

            $.ajax({
                url: '<?= base_url('emergency/emergency/createEmergency') ?>',
                type: 'post',
                dataType: 'json',
                processData: false,                
                contentType: false,
                data: new FormData(frm),
                success: function (response) {
                   
                    Swal.fire({
                        icon:   'success',
                        title:  'Success!',
                        html:   '<p class="h4">Your data is successfully input.</p>'
                    });
                
                $("#category").val("");
                $("#shipment").val("");
                $("#user").val("");
                $("date-time").val("");
                $("#description").val("");
                $('#foto').val("");

                var drEvent = $('.dropify').dropify();
                drEvent = drEvent.data('dropify');
                drEvent.resetPreview();
                drEvent.clearElement();

                },
                error: function () {
                    Swal.fire({
                        icon:   'error',
                        title:  'Failed!',
                        html:   '<p class="h4">Failed to input.</p>'
                    });
                }
            });
        });
    });

</script>