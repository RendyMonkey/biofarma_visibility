s<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx; 
class Retur extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 		$this->load->model('Retur_model');
 		$this->load->model('retur/Retur_model');
 		if(!$this->session->has_userdata('nama')){
 			redirect(base_url("login"));
 		}
 	}

 	public function createRetur()
 	{
		$no_retur = rand();
 		$data = [
 			"no_retur" => $this->input->post('retur_number'),
 			"id_role_retur_from" => $this->session->userdata('id_user_role_lv_2'),
 			"id_role_retur_to" => $this->input->post('id_role_receive'),
 			"time_start_retur" => date("Y-m-d"),
 			"status_retur" => 1,
 			"created_by" => $this->session->userdata('id_user_account'),
 			"created_date" => date("Y-m-d H:i:s"),
 		];
		 
 		$input_data = $this->Retur_model->c_retur($data);
 		if ($this->input->post('detail_retur') != null) {
 			$this->createDetail($this->input->post('detail_retur'));
 		}
 		if($input_data){
 			log_record(null, json_encode($data));
 			echo json_encode(['message' => 'input data berhasil']);
 		}else{
 			echo json_encode(['message' => 'input data gagal']);
 		}
 	}

 	public function createDetail($detail_retur)
 	{
		foreach ($detail_retur as $detail) {
 			$data = [
 				"no_retur" => $detail['no_retur'],
 				"gs_on_id" => $detail['gsOneId'],
 				"uom" => $detail['uom'],
 				"description_retur" => $detail['description'],
 				"created_by" => $this->session->userdata('id_user_account'),
 				"created_date" => date("Y-m-d H:i:s"),
 			];
 			$input_data = $this->Retur_model->c_detail_retur($data);
 		}
 	}

 	public function getRetur()
 	{
 		$startDate = $this->input->post('startDate');
 		$endDate = $this->input->post('endDate');
		$list = $this->Retur_model->r_retur($startDate, $endDate);
		$datas = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $data->no_retur;
			$row[] = $data->batch_id;
			$row[] = $data->nm_role_lvl_2;
			$row[] = $data->status_retur;
			$row[] = $data->time_start_retur;
			$row[] = '<a href='. base_url("retur/retur/detail/").$data->no_retur .' class="d-flex justify-content-center">
					   <i class="las la-clipboard font-18"></i></a>';

			$datas[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->Retur_model->r_retur_count_all(),
                        "recordsFiltered" => $this->Retur_model->r_retur_count_filtered($startDate, $endDate),
                        "data" => $datas,
		); 
 		echo json_encode($result);
 		// $data = $this->Retur_model->r_retur();
 		// echo json_encode($data);
 	}

 	public function getDetailRetur()
 	{
 		$id = $this->input->post('id');
		$list = $this->Retur_model->r_detail_retur($id);
		$datas = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $data->gsOnid;
			$row[] = $data->UOM;
			$row[] = $data->username;
			$row[] = $data->description_retur;

			$datas[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->Retur_model->r_detail_retur_count_all($id),
                        "recordsFiltered" => $this->Retur_model->r_detail_retur_count_filtered($id),
                        "data" => $datas,
		); 
 		echo json_encode($result);
 		// $data = $this->Retur_model->r_retur();
 		// echo json_encode($data);
 	}

	public function getReturWithDetailById($id)
	{
		echo json_encode($this->Retur_model->r_retur_with_detail_by_id($id));
	}

	public function getDetailReturById($id)
	{
		echo json_encode($this->Retur_model->r_detail_retur_by_id($id));
	}
 	public function updateReturWithDetail($id)
 	{
 		$data = [
 			"id_role_retur_from" => $this->input->post('id_role_retur_from'),
 			"id_role_retur_to" => $this->input->post('id_role_retur_to'),
 			"time_start_retur" => $this->input->post('time_start_retur'),
 			"status_retur" => $this->input->post('status_retur')
		 ];
		 
		 $detail_data = [
			"gsOnid" => $this->input->post('gsOnid'),
			"description_retur" => $this->input->post('description_retur'),
			"UOM" => $this->input->post('UOM'),
			"id_user_account_scan_retur_from" => $this->input->post('id_user_account_scan_retur_from')
		];

		 log_record(json_encode($this->Retur_model->r_retur_by_id($id)), json_encode($data));
		 $update_data = $this->Retur_model->u_retur($id, $data);
		 $update_detail_data = $this->Retur_model->u_detail_retur($id, $detail_data);
 		if($update_data){
 			echo json_encode(['message' => 'update data berhasil']);
 		}else{
 			echo json_encode(['message' => 'update data gagal']);
 		}
 	}

 	public function getDetailReturByGsOneId($id){
 		echo json_encode($this->Retur_model->r_detail_retur_by_gsOneId($id));
 	}

 	public function deleteRetur($id)
 	{
 		log_record(json_encode($this->Retur_model->r_retur_by_id($id)), null);
 		$delete_data = $this->Retur_model->d_retur($id);
 		$delete_detail_data = $this->Retur_model->d_detail_retur($id);
 		echo json_encode(['message' => 'delete data berhasil']);
	}

	public function exportReportRetur(){
		$data = $this->Retur_model->r_report_retur();
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'No Retur');
		$sheet->setCellValue('B1', 'Retur to');
		$sheet->setCellValue('C1', 'Time retur');
		$no = 1;
		$x = 2;
		foreach($data as $row)
		{
			$sheet->setCellValue('A'.$x, $no++);
			$sheet->setCellValue('B'.$x, $row->no_retur);
			$sheet->setCellValue('C'.$x, $row->nm_role_lvl_2);
			$sheet->setCellValue('D'.$x, $row->time_start_retur);
			$x++;
		}
		$writer = new Xlsx($spreadsheet);
		$filename = 'Report_retur';
			
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');
	
		$writer->save('php://output');
	}

	 
	//  FOR VIEW
	public function list()
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('list_retur');
	   $this->load->view('_rootComponents/_footer/footer');
	}

	public function detail($id)
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('detail_retur', ['id' => $id]);
	   $this->load->view('_rootComponents/_footer/footer');
	}

	public function create()
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('create_retur');
	   $this->load->view('_rootComponents/_footer/footer');
	}

	public function search()
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('search_retur');
	   $this->load->view('_rootComponents/_footer/footer');
	}
}