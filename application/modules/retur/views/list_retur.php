<link href="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css">
<link href="<?= base_url('assets/plugins/datatables/buttons.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css">
<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.js') ?>"></script>
<div class="page-wrapper">
        <!-- Page Content-->
        <div class="page-content-tab">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">Retur</a></li>
                                    <li class="breadcrumb-item active">List</li>
                                </ol>
                            </div>
                            <h4 class="page-title">List Retur</h4>
                        </div>
                        <!--end page-title-box-->
                    </div>
                    <!--end col-->
                </div><!-- end page title end breadcrumb -->
                <div class="row mb-3">
                        <div class="col-4"><input type="date" id="date-start" class="form-control"></div>
                        <div class="col-4"><input type="date" id="date-end" class="form-control"></div>
                        <div class="col-4">
                            <a href="<?=base_url("retur/exportReportRetur");?>" class="btn btn-success">Export excel</a>
                        </div>
                    </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <table id="datatable" class="table table-bordered dt-responsive nowrap"
                                    style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr class="text-center">
                                            <th>#</th>
                                            <th>No Retur</th>
                                            <th>Batch Id</th>
                                            <th>To User Role level 2</th>
                                            <th>Status Retur</th>
                                            <th>Time Start Retur</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- container -->

<!--  Modal content for the above example -->
            <footer class="footer text-center text-sm-left">&copy; 2020 Biotracking </footer>
            <!--end footer-->
        </div><!-- end page content -->
    </div><!-- end page-wrapper -->

    <script>                                        

       var table = $('#datatable').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?= site_url('retur/getRetur')?>",
          "type": "POST",
          "data": function(data){
            var dateStart = $('#date-start').val();
            var dateEnd = $('#date-form').val();
            data.<?= $this->security->get_csrf_token_name(); ?> = '<?=$this->security->get_csrf_hash();?>';
            data.startDate = dateStart;
            data.endDate = dateEnd;
          }
        },

        //Set column definition initialisation properties.
        "columnDefs": [
          { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
          },
        ],

      });

        $('#date-start').change(function(){
            table.draw();
        });

      $('#date-end').change(function(){
        table.draw();
      });
    </script>