<link href="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/moment/moment.js') ?>"></script>
<script src="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.js') ?>"></script>
<script type="text/javascript" src="https://unpkg.com/@zxing/library@latest"></script>

<link href="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet"
    type="text/css">
<link href="<?= base_url('assets/plugins/datatables/buttons.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css">
<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.js') ?>"></script>
<style type="text/css">
    .myStyle{
        filter: blur(3px);
  -webkit-filter: blur(3px);
    }
body{ 
    width:100%;
   height:100%;
   margin:0;
   background-color:#fff;

}

.load{
  z-index: 999;
  position:absolute;
  max-width:100px;
  margin:0 auto;
  top: 50%;
  left:50%;
  transform: translate(-50%, -50%);
}



/*loading screen*/



.loading-screen{
  float:left;
  height:20px;
  width: 20px;
  margin:0 5px;
  border-radius:50%;
  animation: shrink 1s ease infinite 0ms;
  transform: scale(0.35);
}



/* animation */


.loading-screen:nth-child(1){
  animation: shrink 1s ease infinite 350ms;
  background-color:#45aaf2;
}

.loading-screen:nth-child(2){
  animation: shrink 1s ease infinite 550ms;
  background-color:#ffb8b8;
}

.loading-screen:nth-child(3){
  animation: shrink 1s ease infinite 700ms; 
  background-color:#f9ca24;
}



@keyframes shrink{
  50%{
    -webkit-transform: scale(1);
            transform: scale(1);
        opacity: 1;
  }
  
100%{
  opacity: 0;
}


}

.hidden{
    display: none;
}
</style>
<div class="load hidden">
  <div class="loading-screen"></div>
  <div class="loading-screen"></div>
  <div class="loading-screen"></div>
</div>
<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Retur</a></li>
                                <li class="breadcrumb-item active">create</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Create Retur Order</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <th scope="row" for="category">Retur Number</th>
                                                <td> <input type="text" class="form-control" id="retur_number" placeholder=""></td>
                                            </tr>
                                            <tr>
                                                <th scope="row" for="category">Batch Id</th>
                                                <td> <input type="text" class="form-control" id="bacth_id" placeholder=""></td>
                                            </tr>

                                            <tr>
                                                <th scope="row" for="category">Role Delivery</th>
                                                <td> 
                                                    <select class="form-control" id="receiver">
                                                        <option value="choose">Choose...</option>
                                                    </select>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th scope="row" for="time">Delivery Date</th>
                                                <td> <input type="text" id="date-format" class="form-control"
                                                        placeholder="Saturday 24 June 2017"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-6">
                                <video id="webcam-preview" class="col"></video>
                                <center><button class="btn btn-primary self-align-center col-6" style="position: absolute;top: 40%;left: 27%;" id="scanner" onclick="startScan()">Start Scan</button></center>
                                </div>
                            </div>
                            <table id="datatable" class="table table-bordered dt-responsive nowrap"
                                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr class="text-left">
                                        <th>#</th>
                                        <th>GSOneId</th>
                                        <th>No Retur</th>
                                        <th>UOM</th>
                                        <th>Description Retur</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <button type="button" id="submit" class="btn btn-primary">Submit</button>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- end col -->
    </div><!-- end row -->
</div><!-- container -->
</div><!-- end page content -->
</div><!-- end page-wrapper -->


<script>
var table = $('#datatable').DataTable({
    'scrollX': true,
});
let count = 0;
let arrayGsOneId = [];
let haveScanned = false;
function startScan() {
    const codeReader = new ZXing.BrowserMultiFormatReader();
    codeReader.decodeFromVideoDevice(null, 'webcam-preview', (result, err) => {
        if (result) {
            var ketersediaan = false;
            $.ajax({
                url: '<?= base_url('retur/getDetailReturByGsOneId/') ?>' + result,
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    if(data != null){
                        ketersediaan = true;
                    }
                }
            });

            var scanned = arrayGsOneId.find(function check(gsOneId) {
                 return gsOneId == result;
            });
            document.querySelector(".load").classList.remove("hidden");
            document.querySelector(".page-wrapper").classList.add("myStyle");
            $.ajax({
                url: '<?= base_url('retur/getDetailReturByGsOneId/') ?>' + result,
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    document.querySelector(".load").classList.add("hidden");
                    document.querySelector(".page-wrapper").classList.remove("myStyle");
                    if(data == null){
                        Swal.fire({
                            icon: 'warning',
                            title: 'Item not found !',
                        });
                    }else{
                        arrayGsOneId.push(data.gs_on_id);
                        count += 1;
                        Swal.fire({
                            icon: 'success',
                            title: 'Success!',
                            html: '<p class="h4">Your data with GSOneID '+ result + ' is successfully scanned.</p>'
                        });
                        table.row.add([count, '<input class="form-control" type="text" name="gsOneId[]" value="'+data.gs_on_id+'">', '<input class="form-control" type="text" name="no_retur[]" value="'+data.no_retur+'">', '<input class="form-control" type="text" name="uom[]" value="'+data.uom+'">', '<input class="form-control" type="text" name="description[]" value="'+data.description_retur+'">']).draw();
                    }
                }
            });
        codeReader.reset();
        $('#scanner').show();
    }
});

$('#scanner').hide();
}
 
</script>

<script>
    $('#date-format').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY',
        time: false,
    });

    $.ajax({
            url: '<?= base_url('/user/User_role_level_2/getUserRoleLevel2AllWithoutDatatables') ?>',
            type: 'get',
            dataType: 'json',
            success: function(data){
                var html = '';
                $.each(data, function(key, dataValue){
                    html = '<option value="' + dataValue.id_user_role + '">' + dataValue.nm_role_lvl_2 + '</option>'
                    $("#receiver").append(html);
                });
            }
        });

    // POST TO DATABASE
    $(document).ready(function () {
        var values = [];
        $("#submit").click(function () {
        if(($('#retur_number').val().length < 1) || ($('#receiver').val() == 'choose') || ($('#date-format').val() == '')){
          if($('#retur_number').val().length < 1){
            document.getElementById('retur_number').focus();
            var message = "Retur number is required";
          }else if(document.getElementById('receiver').value == 'choose'){
            var message = "C is required";
          }
          Swal.fire(
              'Input Warning !',
              message,
              'warning'
          );

        }else{
            for(var i = 0; i < document.getElementsByName("gsOneId[]").length; i++) {
                var object = {
                    gsOneId : document.getElementsByName("gsOneId[]")[i].value,
                    no_retur : document.getElementsByName("no_retur[]")[i].value,
                    uom : document.getElementsByName("uom[]")[i].value,
                    description : document.getElementsByName("description[]")[i].value,
                };
                values.push(object);
            }
            var data = {
                <?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>', 
                retur_number: $("#retur_number").val(),
                bacth_id: $("#bacth_id").val(),
                tanggal_pengiriman: $("#date-format").val(),
                id_role_receive: parseInt($("#receiver").val()),
                detail_retur: values
            };      

            $.ajax({
                url: '<?= base_url('retur/createRetur') ?>',
                type: 'post',
                dataType: 'json',
                data: data,
                success: function (response) {
                    Swal.fire({
                        icon:   'success',
                        title:  'Success!',
                        html:   '<p class="h4">Your data is successfully input.</p>'
                    });
                    values = [];
                },
                error: function(){
                    values = [];
                    alert('gagal');
                }
            });
        }
        });
    });
</script>