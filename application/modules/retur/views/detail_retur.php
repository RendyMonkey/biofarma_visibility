<link href="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet"
  type="text/css">
<link href="<?= base_url('assets/plugins/datatables/buttons.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css">
<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.js') ?>"></script>
<div class="page-wrapper">
  <!-- Page Content-->
  <div class="page-content-tab">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <div class="page-title-box">
            <div class="float-right">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0);">Detail Retur Order</a></li>
                <li class="breadcrumb-item active">List</li>
              </ol>
            </div>
            <h4 class="page-title">List Detail Order</h4>
          </div>
          <!--end page-title-box-->
        </div>
        <!--end col-->
      </div><!-- end page title end breadcrumb -->
      <div class="row">
      <div class="col-12">
          <div class="card">
            <div class="card-body">
              <table id="datatable" class="table table-bordered nowrap"
                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>GsOnId</th>
                    <th>UOM</th>
                    <th>User scan</th>
                    <th>Description</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div><!-- end col -->
      </div><!-- end row -->
    </div><!-- container -->

    <!--  Modal content for the above example -->
    <footer class="footer text-center text-sm-left">&copy; 2020 Biotracking <span
        class="text-muted d-none d-sm-inline-block float-right"></i>
        by Mannatthemes</span></footer>
    <!--end footer-->
  </div><!-- end page content -->
</div><!-- end page-wrapper -->

<script>
var data = {
                <?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>', 
                id: <?=$id;?> 
            };     
var table = $('#datatable').DataTable({
              "scrollX": true,
              "processing": true,
              "serverSide": true,
              "order": [],

              "ajax": {
                "url": "<?= site_url('retur/getDetailRetur/')?>",
                "type": "POST",
                "data": data
              },

              "columnDefs": [{
                "targets": [0],
                "orderable": false,
              }, ],

            });
</script>