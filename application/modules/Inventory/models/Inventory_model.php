<?php 
class Inventory_model extends CI_Model{
	public function getData(){
		$query = $this->db->get('user_account');
		return $query->result();
	}

	public function r_inventory_query(){
		$search_attribut = ["ddo.product_name"];
		$column_order = array(null, "ddo.product_name");
		$this->db->select("ddo.product_name, count(ddo.gs_on_id) as total_gsOnId, sum(ddo.qty_vial) as total_vial");
		$this->db->from('detail_delivery_order ddo');
		$this->db->join('detail_delivery_user ddu','ddu.gs_on_id = ddo.gs_on_id ');
		$this->db->where('ddu.id_role', $this->session->userdata('id_user_role_lv_2'));
		$this->db->group_by('ddo.product_name');
		$this->db->order_by('ddo.product_name');
		$i = 0;	
	}

	public function r_inventory(){
		$this->r_inventory_query();
		if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}

	public function r_inventory_report(){
		$this->r_inventory_query();
		return $this->db->get()->result();
	}

	function r_inventory_count_filtered()
    {
        $this->r_inventory_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function r_inventory_count_all()
    {
        $this->r_inventory_query();
        return $this->db->count_all_results();
    }
}
?>