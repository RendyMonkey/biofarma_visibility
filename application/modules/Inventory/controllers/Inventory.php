<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Inventory extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 		if(!$this->session->has_userdata('nama')){
 			redirect(base_url("login"));
 		}
 		$this->load->model('Inventory_model');
 	}

 	public function getInventory()
 	{
		$list = $this->Inventory_model->r_inventory();
		$datas = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $data->product_name;
			$row[] = $data->total_gsOnId;
			$row[] = $data->total_vial;

			$datas[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->Inventory_model->r_inventory_count_all(),
                        "recordsFiltered" => $this->Inventory_model->r_inventory_count_filtered(),
                        "data" => $datas,
		); 
 		echo json_encode($result);
 		// $data = $this->Retur_model->r_retur();
 		// echo json_encode($data);
 	}

 	public function exportReportInventory(){
		$data = $this->Inventory_model->r_inventory_report();
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'No');
		$sheet->setCellValue('B1', 'Product Name');
		$sheet->setCellValue('C1', 'Total GsOneId');
		$sheet->setCellValue('D1', 'Quantity');
		$no = 1;
		$x = 2;
		foreach($data as $row)
		{
			$sheet->setCellValue('A'.$x, $no++);
			$sheet->setCellValue('B'.$x, $row->product_name);
			$sheet->setCellValue('C'.$x, $row->total_gsOnId);
			$sheet->setCellValue('D'.$x, $row->total_vial);
			$x++;
		}
		$writer = new Xlsx($spreadsheet);
		$filename = 'Report_inventory';
			
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');
	
		$writer->save('php://output');
	}
	public function list()
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('inventory');
	   $this->load->view('_rootComponents/_footer/footer');
	}
}
?>