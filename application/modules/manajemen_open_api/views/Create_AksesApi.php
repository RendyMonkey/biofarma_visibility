<link href="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/moment/moment.js') ?>"></script>
<script src="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.js') ?>"></script>
<link href="<?= base_url('assets/plugins/dropify/dropify.min.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/dropify/dropify.min.js') ?>"></script>

<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                
                            <li class="breadcrumb-item"><a href="<?= base_url() ?>">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">Akses API</a></li>
                                    <li class="breadcrumb-item active">Create</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Create Akses</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form class=" col-8 offset-2" enctype="multipart/form-data" id="frm">
                                <div class="form-group">
                                    <label for="category">Partner</label>
                                    <select class="form-control" id="partner">
                                        <option value="choose">Choose...</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="category">URl</label>
                                    <select class="form-control" id="url">
                                        <option value="choose">Choose...</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="validationCustom01">Method</label>
                                    <input type="text" class="form-control" id="method" disabled>
                                </div>
                                <button type="button" id="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container -->
    </div><!-- end page content -->
</div><!-- end page-wrapper -->
<script>
    $(document).ready(function () {
        $.ajax({
            url: '<?= base_url('manajemen_open_api/Partisipan/getUrlWithoutDatatable') ?>',
            type: 'get',
            dataType: 'json',
            success: function(data){
                var html = '';
                $.each(data, function(key, dataValue){
                    html = '<option value="' + dataValue.url + '" data-method="'+dataValue.method+'"  class="test">' + dataValue.url + '</option>'
                    $("#url").append(html);
                }); 
            }
        });

        $("#url").change(function(){
            $('#method').val(document.querySelector('.test').dataset.method);
        });

        $.ajax({
            url: '<?= base_url('/manajemen_open_api/Partisipan/getPartisipanlWithoutDatatable') ?>',
            type: 'get',
            dataType: 'json',
            success: function(data){
                var html = '';
                $.each(data, function(key, dataValue){
                    html = '<option value="' + dataValue.partner_id + '">' + dataValue.partner_name + '</option>'
                    $("#partner").append(html);
                });
            }
        });
        
        $("#submit").click(function () { 
        if(($('#url').val() == 'choose') || ($('#partner').val() == 'choose')){
          if($('#url').val() == 'choose'){
            var message = "Role is required !";
          }else{
            var message = "Partner is required !";
          }
          Swal.fire(
              'Input Warning !',
              message,
              'warning'
          );

        }else{
            $.ajax({
                url: '<?= base_url('manajemen_open_api/Akses_api/createAksesApi') ?>',
                type: 'post',
                dataType: 'json',
                data:{
                    <?=$this->security->get_csrf_token_name();?> : '<?=$this->security->get_csrf_hash();?>',  
                    partner_id: $('#partner').val(),
                    url: $('#url').val(),
                    method: $('#method').val()
                },
                success: function (data) {
                    console.log(data);
                    Swal.fire({
                        icon:   'success',
                        title:  'Success!',
                        html:   '<p class="h4">Your data is successfully input.</p>' 
                    });
                },
                error: function () {
                    Swal.fire({
                        icon:   'error',
                        title:  'Data not created !',
                    });
                }
            });
        }
        });
    });

</script>