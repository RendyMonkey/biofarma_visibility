<link href="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet"
  type="text/css">
<link href="<?= base_url('assets/plugins/datatables/buttons.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css">
<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.js') ?>"></script>
<div class="page-wrapper">
  <!-- Page Content-->
  <div class="page-content-tab">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <div class="page-title-box">
            <div class="float-right">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0);">Partisipan</a></li>
                <li class="breadcrumb-item active">List</li>
              </ol>
            </div>
            <h4 class="page-title">List Partisipan</h4>
          </div>
          <!--end page-title-box-->
        </div>
        <!--end col-->
      </div><!-- end page title end breadcrumb -->
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <table id="datatable" class="table table-bordered dt-responsive nowrap"
                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Partner Name</th>
                    <th>Quote</th>
                    <th>Token</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div><!-- end col -->
      </div><!-- end row -->
    </div><!-- container -->

    <!--  Modal content for the above example -->
    <footer class="footer text-center text-sm-left">&copy; 2020 Biotracking <span
        class="text-muted d-none d-sm-inline-block float-right"></i>
        by Mannatthemes</span></footer>
    <!--end footer-->
  </div><!-- end page content -->
</div><!-- end page-wrapper -->

<script>

var table = $('#datatable').DataTable({

  "processing": true,
  "serverSide": true,
  "order": [],

  "ajax": {
  "url": "<?= site_url('manajemen_open_api/Partisipan/getPartisipan')?>",
  "type": "POST"
},

"columnDefs": [{
  "targets": [0],
  "orderable": false,
}, ],

});

function delete_data(id) {
  Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            url: '<?= base_url('manajemen_open_api/Partisipan/deletePartisipan/') ?>' + id,
            type: 'get',
            dataType: 'json',
            success: function(data) {
              table.ajax.reload(null, false);
              Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
              );              
            },
            error: function () {
              alert('gagal');
            }
          });
        }
      })
}

function reganerate_token(id) {
  Swal.fire({
        title: 'Are you sure?',
        text: "You will regenerate this token!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            url: '<?= base_url('manajemen_open_api/Partisipan/regenerateToken/') ?>' + id,
            type: 'get',
            dataType: 'json',
            success: function(data) {
              table.ajax.reload(null, false);
              Swal.fire(
                'Regenerated !',
                'This token has been regenerated.',
                'success'
              );              
            },
            error: function () {
              alert('gagal');
            }
          });
        }
      })
}


</script>