<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= base_url() ?>">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">Partisipan</a></li>
                                    <li class="breadcrumb-item active">Update Partisipan</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Update Partisipan</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                        <form class="needs-validation" novalidate>
                            <div class="form-row">
                              <div class="col-md-6 mb-3">
                                <label for="validationCustom02">Kode Partisipan</label>
                                <input type="text" class="form-control" id="kode_partisipan" required value="<?= $partisipan->kode_partisipan ?>">
                              </div>
                            </div>
                            <button class="btn btn-primary" type="button" id="submit">Submit form</button>
                          </form>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container -->
    </div><!-- end page content -->
</div><!-- end page-wrapper -->

<script>


$('#submit').click( function(){
        $.ajax({
            url: "<?= base_url('manajemen_open_api/Partisipan/updatePartisipan/').$partisipan->id_partisipan; ?>",
            type: 'post',
            dataType: 'json',
            data: {
                <?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>',
                kode_partisipan : $('#kode_partisipan').val(),
            },
            success: function (data) {
                Swal.fire(
                'Data has been updated!',
                'Your data is successfuly updated.',
                'success'
                );
            }, error: function(){
                Swal.fire(
                'Data not created!',
                'Please contact developer to fix it.',
                'error'
                )
            }
        })
       });
</script>