a<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Akses_api extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 		$this->load->model('Akses_api_model');
 		if(!$this->session->has_userdata('nama')){
 			redirect(base_url("login"));
 		}
 	}

 	public function createAksesApi()
 	{
 		$data = [
			 "partner_id" => $this->input->post('partner_id'),
			 "url" => $this->input->post('url'),
			 "method" => $this->input->post('method'),
 		];
 		$input_data = $this->Akses_api_model->c_akses_api($data);
 		if($input_data){
 			log_record(null, json_encode($data));
 			echo json_encode(['message' => 'input data berhasil']);
 		}else{
 			echo json_encode(['message' => 'input data gagal']);
 		}
 	}

 	public function getAksesApi()
	 { 
		$list = $this->Akses_api_model->r_akses_api();
		$datas = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $data->partner_name;//sementara !!!
			$row[] = $data->url;//sementara !!!
			$row[] = $data->method;//sementara !!!
			$row[] = $data->token_expired_date;//sementara !!!
			$row[] = '<a href="'.base_url('manajemen_open_api/Akses_api/update/').'" class="mr-2">
					   	<i class="las la-pen text-info font-18"></i>
					   </a>
                    <a onclick="delete_data()" href="#" class="delete mr-2" data-id="" >
					 <i class="las la-trash-alt text-danger font-18 del" data-id=""></i>
												';

			$datas[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->Akses_api_model->r_akses_api_count_all(),
                        "recordsFiltered" => $this->Akses_api_model->r_akses_api_count_filtered(),
                        "data" => $datas,
		); 
 		echo json_encode($result);
 	}

 	public function deleteAksesApi($id)
 	{
 		log_record(json_encode($this->Akses_api_model->r_akses_api_by_id($id), null));
 		$delete = $this->Akses_api_model->d_akses_api($id);
 		echo json_encode(['message' => 'delete data berhasil']);
	}

	public function updateAksesApi($id)
 	{
 		$data = [
 			"id_url" => $this->input->post('id_url'),
			"id_partisipan" => $this->input->post('id_partisipan'),
			// "updated_by" => $this->session->userdata('id_user_account'),
			// "update_date" => date("Y-m-d H:i:s"), 
 		];
		log_record(json_encode($this->Akses_api_model->r_akses_api_by_id($id)), json_encode($data));
		$data = $this->security->xss_clean($data);
 		$update = $this->Akses_api_model->u_akses_api($id, $data);
 		if($update){
 			echo json_encode(['message' => 'update data berhasil']);
 		}else{
 			echo json_encode(['message' => 'update data gagal']);
 		}
	 }
	 
	 public function list()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('list_aksesApi');
		$this->load->view('_rootComponents/_footer/footer');
	 }
 
	//  public function detail($id)
	//  {
	// 	$this->load->view('_rootComponents/_header/heder');
	// 	$this->load->view('_rootComponents/_sidebar/sidebar');
	// 	$this->load->view('_rootComponents/_navbar/navbar');
	// 	$this->load->view('detail_delivery', ['id' => $id]);
	// 	$this->load->view('_rootComponents/_footer/footer');
	//  }
 
	 public function create()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('create_aksesApi');
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function update($id)
	 {
		$data = $this->Akses_api_model->r_akses_api_join($id);
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('update_akses_api', ['data' => $data]);
		$this->load->view('_rootComponents/_footer/footer');
	 }
	 
}