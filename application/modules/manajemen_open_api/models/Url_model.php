<?php 
class Url_model extends CI_Model{

	public function r_url_query(){
		$search_attribut = ["url", "method"];
		$column_order = array(null, "url", "method");
		search_datatable("service", $search_attribut, $column_order);
	}

	public function r_url() 
	{
		$this->r_url_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}

	function r_url_count_filtered()
    {
        $this->r_url_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function r_url_without_datatable(){
        return $this->db->get('service')->result();
    }
 
    public function r_url_count_all()
    {
        $this->db->from('service');
        return $this->db->count_all_results();
    }

    public function r_url_where_not_in($id){
        $this->db->from('service');
        return $this->db->where_not_in('id_url', $id)->get()->result();
    }
}
?>