<?php 
class Akses_api_model extends CI_Model{

	public function c_akses_api($data){
		$query = $this->db->insert('partner_service', $data);
		return $query;
	}

	public function r_akses_api_query(){
		$search_attribut = ["partner_service.url", "partner_service.method", "partner.token_expired_date", "partner.partner_name"];
		$column_order = array(null, "partner_service.url", "partner_service.method", "partner.token_expired_date",  "partner.partner_name");
		$this->db->select("partner_service.url, partner_service.method, partner.token_expired_date, partner.partner_name");
		$this->db->from('partner');
		$this->db->join('partner_service', 'partner_service.partner_id = partner.partner_id');
		$i = 0;
		foreach($search_attribut as $search) {
			if($_POST['search']['value'])
			{
				if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($search, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($search, $_POST['search']['value']);
                }

                if(count($search_attribut) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
			}			
            $i++;
		}

		if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function r_akses_api() 
	{
		$this->r_akses_api_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}

	function r_akses_api_count_filtered()
    {
        $this->r_akses_api_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function r_akses_api_count_all()
    {
        $this->db->from('partner');
		$this->db->join('partner_service', 'partner_service.partner_id = partner.partner_id');
        return $this->db->count_all_results();
    }

	public function u_akses_api($id, $data){
		$this->db->where('partner_id', $id);
		$query = $this->db->update('partner', $data);
		return $query;
	}

	public function d_akses_api($id){
		$query = $this->db->delete('partner', array('partner_id' => $id));
		return $query;
	}

	public function r_akses_api_by_id($id){
		return $this->db->where_get('partner_service', ['partner_id' => $id])->row();
	}

	// public function r_akses_api_join($id){
	// 	$this->db->select("partner_service.url, partner_service.method, partner.token_expired_date, partner.partner_name");
	// 	$this->db->from('partner');
	// 	$this->db->join('partner_service', 'partner_service.partner_id = partner.partner_id');
	// 	return $this->db->where('partner.partner_id', $id)->get()->row();  
	// }
}
?>