   <!-- Log In page -->
    <div class="container">
        <div class="row vh-100 d-flex justify-content-center">
            <div class="col-12 align-self-center">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-5 mx-auto">
                            <div class="card">
                                <div class="card-body">
                                    <div class="media mb-3"><a href="../analytics/analytics-index.html"
                                            class="logo logo-admin"><img src="<?= base_url('/assets/images/logo-sm.png') ?>" alt="logo"
                                                class="auth-logo" height="40"></a>
                                        <div class="media-body align-self-center text-truncate ml-2">
                                            <h4 class="mt-0 mb-1 font-weight-semibold text-dark font-18">Register for new user</h4>
                                            <p class="text-muted mb-0">Get your account now.</p>
                                        </div>
                                        <!--end media-body-->
                                    </div>
                                    <form class="form-horizontal auth-form my-4" action="index.html">
                                        <div class="form-group"><label for="username">Username</label>
                                            <div class="input-group mb-3"><input type="text" class="form-control"
                                                    name="username" id="username" placeholder="Enter username"></div>
                                        </div>
                                        <!--end form-group-->
                                        <div class="form-group"><label for="useremail">Email</label>
                                            <div class="input-group mb-3"><input type="email" class="form-control"
                                                    name="useremail" id="user_account_email" placeholder="Enter Email"></div>
                                        </div>
                                        <!--end form-group-->

                                        <div class="form-group"><label for="mo_number">Full Name</label>
                                            <div class="input-group mb-3"><input type="text" class="form-control"
                                                    name="mobile-number" id="nm_lengkap"
                                                    placeholder="Enter Mobile Number"></div>
                                        </div>

                                        <div class="form-group"><label for="userpassword">Password</label>
                                            <div class="input-group mb-3"><input type="password" class="form-control"
                                                    name="password" id="password" placeholder="Enter password">
                                            </div>
                                        </div>
                                        <!--end form-group-->
                                        <div class="form-group"><label for="conf_password">Address</label>
                                            <div class="input-group mb-3"><input type="text" class="form-control"
                                                    name="conf-password" id="address"
                                                    placeholder="Enter Confirm Password"></div>
                                        </div>
                                        <!--end form-group-->
                                        <div class="form-group"><label for="mo_number">Mobile Number</label>
                                            <div class="input-group mb-3"><input type="text" class="form-control"
                                                    name="mobile-number" id="no_telphone"
                                                    placeholder="Enter Mobile Number"></div>
                                        </div>
                                        
                                        <!--end form-group-->
                                        <div class="form-group mb-0 row">
                                            <div class="col-12 mt-2"><button
                                                    class="btn btn-primary btn-block waves-effect waves-light"
                                                    type="button" id="submit">Register <i
                                                        class="fas fa-sign-in-alt ml-1"></i></button></div>
                                            <!--end col-->
                                        </div>
                                        <!--end form-group-->
                                    </form>
                                    <!--end form-->
                                    <div class="m-3 text-center text-muted">
                                        <p class="">Already have an account ? <a
                                                href="<?= base_url('login/login') ?>" class="text-primary ml-2">Log
                                                in</a></p>
                                    </div>
                                </div>
                                <!--end card-body-->
                            </div>
                            <!--end card-->
                        </div>
                        <!--end col-->
                    </div>
                    <!--end row-->
                </div>
                <!--end card-body-->
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
