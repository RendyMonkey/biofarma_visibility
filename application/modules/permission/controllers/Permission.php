<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Permission extends CI_Controller {

	function __construct(){
 		parent::__construct();
 		$this->load->model('Permission_model');
 		$this->load->helper('url');
 		if(!$this->session->has_userdata('nama')){
 			redirect(base_url("login"));
 		}
 	}

 	//NOTE : untuk parameter type ada 2 yaitu permission_web atau permission_mobile
	public function createPermission($type)
 	{
 		$data = [
 			"id_role" => $this->input->post('id_role'),
 			"id_menu" => $this->input->post('id_menu')
 		];
 		$input = $this->Permission_model->c_permission($type, $this->security->xss_clean($data));
 		if($input){
 			log_record(null ,json_encode($data));
 			echo json_encode(['message' => 'input data berhasil']);
 		}else{
 			echo json_encode(['message' => 'input data gagal']);
 		}
 	}

 	public function getPermission($type)
 	{
 		$data = $this->Permission_model->r_permission($type);
 		echo json_encode($data);
 	}

 	public function updatePermission($type, $id_permission)
 	{
 		$data = [
 			"id_role" => $this->input->post('id_role'),
 			"id_menu" => $this->input->post('id_menu')
 		];

 		log_record(json_encode($this->Permission_model->r_permission_by_id($type, $id_permission)),json_encode($data));
 		$update = $this->Permission_model->u_permission($type, $id_permission, $this->security->xss_clean($data));
 		if($update){
 			echo json_encode(['message' => 'update data berhasil']);
 		}else{
 			echo json_encode(['message' => 'update data gagal']);
 		}
 	}

 	public function deletePermission($type, $id_permission)
 	{
 		log_record(json_encode($this->Permission_model->r_permission_by_id($type, $id_permission)), null);
 		$delete = $this->Permission_model->d_permission($type, $id_permission);
 		echo json_encode(['message' => 'delete data berhasil']);
	}
	
	// FOR VIEW 
	// public function list() 
	// {
	// 	$this->load->view('_rootComponents/_header/heder');
	// 	$this->load->view('_rootComponents/_sidebar/sidebar');
	// 	$this->load->view('_rootComponents/_navbar/navbar');
	// 	$this->load->view('list_permission');
	// 	$this->load->view('_rootComponents/_footer/footer');
	// }

	// public function create() 
	// {
	// 	$this->load->view('_rootComponents/_header/heder');
	// 	$this->load->view('_rootComponents/_sidebar/sidebar');
	// 	$this->load->view('_rootComponents/_navbar/navbar');
	// 	$this->load->view('create_permission');
	// 	$this->load->view('_rootComponents/_footer/footer');
	// }
	public function getPermissionWeb()
 	{
		$list = $this->Permission_model->r_permission_web();
		$all_data = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$row = array();
			$no++;
			$row[] = $no;
			$row[] = $data->nama_menu;
			$row[] = $data->nm_role;
			$row[] = '<a href="'. base_url("permission/updatePermissionWeb/").$data->id_permission_web.'" class="mr-2">
					   	<i class="las la-pen text-info font-18"></i>
					   </a> 
                    <a href="#" onclick="delete_data('.$data->id_permission_web.')">
					 <i class="las la-trash-alt text-danger font-18"></i>
												</a>';

			$all_data[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->Permission_model->r_permission_web_count_all(),
                        "recordsFiltered" => $this->Permission_model->r_permission_web_count_filtered(),
                        "data" => $all_data,
		); 
 		echo json_encode($result);
	 }

	 public function getPermissionMobile()
 	 {
		$list = $this->Permission_model->r_permission_mobile();
		$all_data = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$row = array();
			$no++;
			$row[] = $no;
			$row[] = $data->nama_menu;
			$row[] = $data->nm_role;
			$row[] = '<a href="'. base_url("permission/updatePermissionMobile/").$data->id_permission_mobile.'" class="mr-2">
					   	<i class="las la-pen text-info font-18"></i>
					   </a> 
                    <a href="#" onclick="delete_data('.$data->id_permission_mobile.')">
					 <i class="las la-trash-alt text-danger font-18"></i>
												</a>';

			$all_data[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->Permission_model->r_permission_mobile_count_all(),
                        "recordsFiltered" => $this->Permission_model->r_permission_mobile_count_filtered(),
                        "data" => $all_data,
		); 
 		echo json_encode($result);
	 }
	 
	public function listPermissionWeb()
	{
		$data = $this->Permission_model->r_permission('permission_web');
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('permission_web/list_permission_web', ['data' => $data]);
		$this->load->view('_rootComponents/_footer/footer');
	}

	public function createPermissionWeb()
	{
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('permission_web/create_permission_web');
		$this->load->view('_rootComponents/_footer/footer');
	}

	public function updatePermissionWeb($id)
	{
		$data = $this->Permission_model->r_permission_join_menu_role_by_id($id);
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('permission_web/update_permission_web', ['data' => $data]);
		$this->load->view('_rootComponents/_footer/footer');
	}

	public function listPermissionMobile()
	{
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('permission_mobile/list_permission_mobile');
		$this->load->view('_rootComponents/_footer/footer');
	}

	public function createPermissionMobile()
	{
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('permission_mobile/create_permission_mobile');
		$this->load->view('_rootComponents/_footer/footer');
	}

	public function updatePermissionMobile($id)
	{	$data = $this->Permission_model->r_permission_mobile_join_menu_role_by_id($id);
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('permission_mobile/update_permission_mobile', ['data' => $data]);
		$this->load->view('_rootComponents/_footer/footer');
	}

}	
?>