<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Delivery_order extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 		$this->load->model('Delivery_order_model');
 		$this->load->model('User/User_account_model');
 		if(!$this->session->has_userdata('nama')){
 			redirect(base_url("login"));
 		}
 	}

 	public function createDeliveryWithDetailOrder()
 	{
		$no_do = rand();
		$no_request = rand();
 		$data = [
			"tanggal_pengiriman" => $this->input->post('tanggal_pengiriman'),
			"time_start_delivery" => date("Y-m-d H:i:s"),	
 			"status" => 1,
 			"no_request" => $this->input->post('no_request'),
 			"no_do" => $this->input->post('no_delivery'),
 			"id_role_delivery" => $this->session->userdata('id_user_role_lv_2'),
 			"id_role_receive" => $this->input->post('id_role_receive'),
 			"eta" => $this->input->post('ETA'),
			"qty_delivery" => $this->input->post('qty_delivery'),
			"created_by" => $this->session->userdata('id_user_account'),
			"created_date" => date("Y-m-d H:i:s"),
			"parent_do" => $this->input->post('parent_do'),			   
			"uom" => $this->input->post('uom'),			   
 		];
 		$input_data = $this->Delivery_order_model->c_delivery_order($data);
 		if ($this->input->post('detail_order') != null) {
 			$this->createDetail($this->input->post('detail_order'));
 		}
 		if($input_data){
 			echo json_encode($this->input->post('detail_order'));
 		}else{
 			echo json_encode(['message' => 'input data gagal']);
 		}
 	}

 	public function createDetail($detail_order){
 		foreach ($detail_order as $detail) {
 			$data = [
 				"no_do" => $detail['no_do'],
 				"gs_on_id" => $detail['gsOneId'],
 				"created_by" => $this->session->userdata('id_user_account'),
 				"created_date" => date("Y-m-d H:i:s"),
 				"product_model_id" => $detail['product_model_id'],
 				"product_name" => $detail['product_name'],
 				"gs_one_inner_box" => $detail['gs_one_inner_box'],
 				"gs_one_master_box" => $detail['gs_one_master_box'],
 				"qty_inner_box" => $detail['qty_inner_box'],
 				"qty_vial" => $detail['qty_vial'],
 			];
 			$input_data = $this->Delivery_order_model->c_detail_delivery_order($data);
 		}
 		if($input_data){
 			return true;
 		}else{
 			return false;
 		}
 	}

 	public function createDetailWithoutParent(){
 		foreach ($this->input->post('detail_order') as $detail) {
 			$data = [
 				"no_do" => $detail['no_do'],
 				"gs_on_id" => $detail['gsOneId'],
 				"created_by" => $this->session->userdata('id_user_account'),
 				"created_date" => date("Y-m-d H:i:s"),
 				"product_model_id" => $detail['product_model_id'],
 				"product_name" => $detail['product_name'],
 				"gs_one_inner_box" => $detail['gs_one_inner_box'],
 				"gs_one_master_box" => $detail['gs_one_master_box'],
 				"qty_inner_box" => $detail['qty_inner_box'],
 				"qty_vial" => $detail['qty_vial'],
 			];
 			$input_data = $this->Delivery_order_model->c_detail_delivery_order($data);
 		}
 		if($input_data){
 			echo json_encode($this->input->post('detail_order'));
 		}else{
 			echo json_encode(['message' => 'input data gagal']);
 		}
 	}

 	public function getDeliveryOrder()
	 { 
	 	$startDate = $this->input->post('startDate');
 		$endDate = $this->input->post('endDate');
		$list = $this->Delivery_order_model->r_delivery_order($startDate, $endDate);
		$datas = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $data->no_do;
			$row[] = $data->no_request;
			$row[] = $data->delivery;
			$row[] = $data->receive;
			$row[] = $data->tanggal_pengiriman;
			if($data->status == 1){
				$row[] = "Dalam pengiriman";
			}else if($data->status == 2){
				$row[] = "Sedang dalam penerimaan";
			}else{
				$row[] = "Sudah diterima";
			}
			$row[] = $data->qty_delivery;
			$row[] = '<a href="'.base_url('delivery_order/update/').$data->no_do.'" class="mr-2">
					   	<i class="las la-pen text-info font-18"></i>
					   </a>
					   <a href="'.base_url('delivery_order/detail/').$data->no_do.'" class="mr-2">
					   	<i class="las la-clipboard text-info font-18"></i>
					   </a> 
                    <a onclick="delete_data('.$data->no_do.')" href="#" class="delete" data-id="'.$data->no_do.'" >
					 <i class="las la-trash-alt text-danger font-18 del" data-id="'.$data->no_do.'"></i>
												</a>';

			$datas[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->Delivery_order_model->r_delivery_order_count_all(),
                        "recordsFiltered" => $this->Delivery_order_model->r_delivery_order_count_filtered($startDate, $endDate),
                        "data" => $datas,
		); 
 		echo json_encode($result);
	 }

	 public function getDetailDeliveryOrder()
	 { 
	 	$id = $this->input->post('id');
		$list = $this->Delivery_order_model->r_detail_delivery_order($id);
		$datas = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $data->gs_on_id;
			$row[] = $data->uom;
			$row[] = $data->imei_user;
			$row[] = $data->username;
			$row[] = $data->status_received;
			$row[] = $data->product_model_id;
			$row[] = $data->product_name;

			$datas[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->Delivery_order_model->r_detail_delivery_order_count_all($id),
                        "recordsFiltered" => $this->Delivery_order_model->r_detail_delivery_order_count_filtered($id),
                        "data" => $datas,
		); 
 		echo json_encode($result);
	 }
	 
	 public function getDeliveryOrderStatusEmergency()
	 { 
		$list = $this->Delivery_order_model->r_delivery_order_status_emergency();
		$datas = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $data->no_do;
			$row[] = $data->no_request;
			$row[] = $this->db->query('SELECT * FROM user_role_level_2 WHERE id_user_role ='.$data->id_role_delivery)->row()->nm_role_lvl_2;
			$row[] = $data->nm_role_lvl_2;
			$row[] = $data->tanggal_pengiriman;
			if($data->status == 1){
				$row[] = "Dalam pengiriman";
			}else if($data->status == 2){
				$row[] = "Sedang dalam penerimaan";
			}else{
				$row[] = "Sudah diterima";
			}
			$row[] = $data->qty_delivery;
			$row[] = '<a href="'.base_url('delivery_order/resend/').$data->no_do.'" class="mr-2">
					   	<i class="las la-pen text-info font-18"></i>
					   </a> ';
			$datas[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->Delivery_order_model->r_delivery_order_count_filtered_status_emergency(),
                        "recordsFiltered" => $this->Delivery_order_model->r_delivery_order_count_filtered_status_emergency(),
                        "data" => $datas,
		); 
 		echo json_encode($result);
 	}

 	public function getDeliveryWithDetailOrderById($id)
 	{
 		echo json_encode($this->Delivery_order_model->r_delivery_order_with_detail_by_id($id));
 	}

 	public function getDetailDeliveryOrderById($id)
 	{
 		echo json_encode($this->Delivery_order_model->r_detail_delivery_order_by_id($id));
 	}

 	public function updateDeliveryWithDetailOrder($id)
 	{
		$time_finish_receive = null;
		if($this->input->post('status') == 3){
			$time_finish_receive = date("Y-m-d");	
		}
		$data = [
			"tanggal_pengiriman" => $this->input->post('tanggal_pengiriman'),
			"status" => $this->input->post('status'),
			"id_role_receive" => $this->input->post('id_role_receive'),
			"eta" => $this->input->post('ETA'),
			"qty_delivery" => $this->input->post('qty_delivery'),
			"updated_by" => $this->session->userdata('id_user_account'),
			"updated_date" => date("Y-m-d H:i:s"),
		];

 		$update_data = $this->Delivery_order_model->u_delivery_order($id, $data);
 		if($update_data){
 			echo json_encode(['message' => 'update data berhasil']);
 		}else{
 			echo json_encode(['message' => 'update data gagal']);
 		}
	 }
	 
	 public function resendDelivery($id)
 	{
		$data = [
			"status" => 1,
			"eta" => $this->input->post('ETA'),
			"updated_by" => $this->session->userdata('id_user_account'),
			"updated_date" => date("Y-m-d H:i:s"),
		];

 		$update_data = $this->Delivery_order_model->u_delivery_order($id, $data);
 		if($update_data){
 			echo json_encode(['message' => 'update data berhasil']);
 		}else{
 			echo json_encode(['message' => 'update data gagal']);
 		}
 	}

 	public function deleteDeliveryWithDetailOrder($id)
 	{
 		$delete = $this->Delivery_order_model->d_delivery_order($id);
 		$delete_detail = $this->Delivery_order_model->d_detail_delivery_order($id);
 		echo json_encode(['message' => 'delete data berhasil']);
	}

	public function getDeliveryOrderWithoutDatatable(){
		echo json_encode($this->Delivery_order_model->r_delivery_order_without_datatable());
	}

	public function getDetailDeliveryOrderByGsOneId($id){
		echo json_encode($this->Delivery_order_model->r_detail_delivery_order_by_gsOneId($id));
	}

	public function exportReportReceiver(){
		$data = $this->recooling_model->r_reccoling_report();
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'No');
		$sheet->setCellValue('B1', 'Kategori Recooling');
		$sheet->setCellValue('C1', 'Description');
		$sheet->setCellValue('D1', 'Time Reccoling');
		$sheet->setCellValue('E1', 'User');
		$no = 1;
		$x = 2;
		foreach($data as $row)
		{
			$sheet->setCellValue('A'.$x, $no++);
			$sheet->setCellValue('B'.$x, $row->category);
			$sheet->setCellValue('C'.$x, $row->description);
			$sheet->setCellValue('D'.$x, $row->time_recooling);
			$sheet->setCellValue('E'.$x, $row->username);
			$x++;
		}
		$writer = new Xlsx($spreadsheet);
		$filename = 'Report_recooling';
			
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');
	
		$writer->save('php://output');
	}

	public function exportReportDeliveryOrder(){
		$data = $this->Delivery_order_model->r_report_delivery_order();
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'No');
		$sheet->setCellValue('B1', 'No Delivery');
		$sheet->setCellValue('C1', 'Request Number');
		$sheet->setCellValue('D1', 'Delivery From');
		$sheet->setCellValue('E1', 'Ship To');
		$sheet->setCellValue('F1', 'Date Delivery');
		$sheet->setCellValue('G1', 'Status');
		$no = 1;
		$x = 2;
		foreach($data as $row)
		{
			$sheet->setCellValue('A'.$x, $no++);
			$sheet->setCellValue('B'.$x, $row->no_do);
			$sheet->setCellValue('C'.$x, $row->no_request);
			$sheet->setCellValue('D'.$x, $row->delivery);
			$sheet->setCellValue('E'.$x, $row->receive);
			$sheet->setCellValue('F'.$x, $row->tanggal_pengiriman);
			if($row->status == "1"){
				$sheet->setCellValue('G'.$x, "Dalam pengiriman");
			}else if($row->status == "2"){
				$sheet->setCellValue('G'.$x, "Sedang dalam penerimaan");
			}else{
				$sheet->setCellValue('G'.$x, "Sudah diterima");
			}
			$x++;
		}
		$writer = new Xlsx($spreadsheet);
		$filename = 'Report_delivery';
			
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');
	
		$writer->save('php://output');
	}

	public function list()
	{
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('list_delivery');
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function list_pending()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('list_pending_delivery');
		$this->load->view('_rootComponents/_footer/footer');
	 }
 
	 public function detail($id)
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('detail_delivery', ['id' => $id]);
		$this->load->view('_rootComponents/_footer/footer');
	 }
 
	 public function create()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('create_delivery');
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function update($id)
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('update_delivery', ['id' => $id]);
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function resend($id)
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('resend_delivery', ['id' => $id]);
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function trackBackByGsOne()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('track_back_by_GsOne');
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function trackBackByNoDo()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('track_back_by_NoDo');
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function scanDelivery()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('scan_delivery_order');
		$this->load->view('_rootComponents/_footer/footer');
	 }
	 public function getDeliveryOrderByIdJoinUserLevel2($id){
		echo json_encode($this->Delivery_order_model->r_delivery_order_join_user_rol_lvl_2_byId($id));
	 }

	 public function getDetailDeliveryOrderByNoDoWhereStatusDalamPerjalanan($no_do){
	 	echo json_encode($this->Delivery_order_model->r_delivery_order_by_no_do_where_status_dalam_perjalanan($no_do));
	 }

	 public function test(){
	 	echo json_encode($this->Delivery_order_model->r_trackback_delivery_order('0182903'));
	 }

	 public function getLocationWarehouse(){
	 	$all_location = [];
	 	foreach ($this->Delivery_order_model->r_disribution_location() as $value) {
	 		$data = [];
	 		$data[] = $value->nm_role_lvl_2;
	 		$data[] = explode(',', $value->location_warehouse)[0];
	 		$data[] = explode(',', $value->location_warehouse)[1];

	 		$all_location[] = $data;
	 	}
	 	echo json_encode($all_location);
	 	// echo json_encode($this->Delivery_order_model->r_disribution_location());	
	 }

	 public function scanDeliveryOrder(){
 		$input_data = $this->Delivery_order_model->u_status_delivery_order($this->input->post('no_do')['text']);
 		if($input_data){
 			echo json_encode(['message' => 'input data berhasil']);
 		}else{
 			echo json_encode(['message' => 'input data gagal']);
 		}
 	}

 	public function getReportDeliveryStatus(){
 		echo json_encode($this->Delivery_order_model->order_report_quantity());
	}
}