<?php 
class Delivery_order_model extends CI_Model{

	public function c_delivery_order($data){
		$query = $this->db->insert('delivery_order', $data);
		return $query;
	}

	public function r_detail_delivery_order_query($id){
		$search_attribut = ["detail_delivery_order.gs_on_id", "detail_delivery_order.uom", "detail_delivery_user.imei_user", "user_account.username","detail_delivery_order.status_received", "detail_delivery_order.product_model_id", "detail_delivery_order.product_name"];
		$column_order = array(null, "detail_delivery_order.gs_on_id", "detail_delivery_order.uom", "detail_delivery_user.imei_user", "user_account.username","detail_delivery_user.status_received", "detail_delivery_order.product_model_id", "detail_delivery_order.product_name");
		$this->db->select("detail_delivery_order.gs_on_id, detail_delivery_order.uom, detail_delivery_user.imei_user, user_account.username, detail_delivery_order.status_received, detail_delivery_order.product_model_id, detail_delivery_order.product_name");
		$this->db->from('detail_delivery_order');
		$this->db->join('detail_delivery_user', 'detail_delivery_user.gs_on_id = detail_delivery_order.gs_on_id');
		$this->db->join('user_account', 'user_account.id_user_account = detail_delivery_user.id_user');
		$this->db->where('detail_delivery_order.no_do', $id);
		$i = 0;
	}

	public function r_delivery_order_query($startDate ="", $endDate=""){
		$search_attribut = ["a.no_do", "a.no_request", "a.tanggal_pengiriman", "a.status", "a.qty_delivery", "b.nm_role_lvl_2", "c.nm_role_lvl_2"];
		$column_order = array(null, "a.no_do", "a.no_request", "a.tanggal_pengiriman", "a.status", "a.qty_delivery", "b.nm_role_lvl_2", "c.nm_role_lvl_2");
		$this->db->select("a.no_do, a.no_request, a.tanggal_pengiriman, a.status, a.qty_delivery, b.nm_role_lvl_2 as delivery, c.nm_role_lvl_2 as receive");
		$this->db->from('delivery_order a');
		$this->db->join('user_role_level_2 b', 'b.id_user_role = a.id_role_delivery');
		$this->db->join('user_role_level_2 c', 'c.id_user_role = a.id_role_receive');
		if ($startDate != "" AND $endDate != "") {
			$this->db->where('a.created_date >=', $startDate);
			$this->db->where('a.created_date <=', $endDate);
		}else if($startDate != "" AND $endDate == ""){
			$this->db->where('a.created_date >=', $startDate);
		}else if($startDate == "" AND $endDate != ""){
			$this->db->where('a.created_date <=', $endDate);
		}
		$i = 0;
	}


	public function r_report_delivery_order(){
		$this->r_delivery_order_query();
		$query = $this->db->get();
		return $query->result();
	}
	public function r_delivery_order($startDate, $endDate) 
	{
		$this->r_delivery_order_query($startDate, $endDate);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}

	public function r_detail_delivery_order($id) 
	{
		$this->r_detail_delivery_order_query($id);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}

	public function r_delivery_order_status_emergency() 
	{
		$this->r_delivery_order_query_status_emergency();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}

	function r_delivery_order_count_filtered($startDate, $endDate)
    {
        $this->r_delivery_order_query($startDate, $endDate);
        $query = $this->db->get();
        return $query->num_rows();
    }
 	
 	function r_detail_delivery_order_count_filtered($id)
    {
        $this->r_detail_delivery_order_query($id);
        $query = $this->db->get();
        return $query->num_rows();
    }

	function r_delivery_order_count_filtered_status_emergency()
    {
        $this->r_delivery_order_query_status_emergency();
        $query = $this->db->get();
        return $query->num_rows();
	}
	
    public function r_delivery_order_count_all()
    {
        $this->db->from('delivery_order');
        return $this->db->count_all_results();
	}

	public function r_detail_delivery_order_count_all($id)
    {
        $this->db->from('detail_delivery_order');
        $this->db->where('no_do', $id);
        return $this->db->count_all_results();
	}
	
	public function r_delivery_order_count_all_status_emergency()
    {
        $this->db->from('delivery_order');
        $this->db->where('is_emergency', true);
        return $this->db->count_all_results();
    }

	public function r_delivery_order_with_detail_by_id($id)
	{
		return $this->db->query("SELECT delivery_order.*,detail_delivery_order.* FROM delivery_order JOIN detail_delivery_order ON delivery_order.no_do=detail_delivery_order.no_do WHERE delivery_order.no_do=".$id)->row();
	}

	public function u_delivery_order($id, $data){
		$this->db->where('no_do', $id);
		$query = $this->db->update('delivery_order', $data);
		return $query;
	}

	public function d_delivery_order($id){
		$query = $this->db->delete('delivery_order', array('no_do' => $id));
		return $query;
	}

	public function c_detail_delivery_order($data){
		$query = $this->db->insert('detail_delivery_order', $data);
		return $query;
	}

	// public function r_detail_delivery_order(){
	// 	$query = $this->db->get('detail_delivery_order');
	// 	return $query->result();
	// }

	public function r_detail_delivery_order_by_id($id){
		return $this->db->get_where('detail_delivery_order', ['no_do' => $id])->result();
	}

	public function u_detail_delivery_order($id, $data){
		$this->db->where('no_do', $id);
		$query = $this->db->update('detail_delivery_order', $data);
		return $query;
	}

	public function d_detail_delivery_order($id){
		$query = $this->db->delete('detail_delivery_order', array('no_do' => $id));
		return $query;
	}

	public function r_received_delivery_order(){
		return $this->db->query("SELECT * FROM delivery_order WHERE status=3")->result();
	}

	public function r_delivery_order_without_datatable(){
		return $this->db->get('delivery_order')->result();
	}

	public function r_detail_order_without_datatable(){
		return $this->db->get('delivery_order')->result();
	}

	public function r_delivery_order_join_user_rol_lvl_2_byId($id){
		$this->db->from('delivery_order');
		$this->db->join('user_role_level_2', 'user_role_level_2.id_user_role = delivery_order.id_role_receive');
		return $this->db->where('delivery_order.no_do', $id)->get()->row();
	}

	public function r_detail_delivery_order_by_gsOneId($id){
		return $this->db->get_where('detail_delivery_order', ['gs_on_id' => $id])->row();
	}

	public function r_disribution_location(){
		$this->db->select("user_role_level_2.nm_role_lvl_2, user_role_level_2.location_warehouse, SUM(delivery_order.qty_delivery)");
		$this->db->from("user_role_level_2");
		$this->db->group_by("user_role_level_2.nm_role_lvl_2");
		$this->db->group_by("user_role_level_2.location_warehouse");
		return $this->db->join("delivery_order", "delivery_order.id_role_receive = user_role_level_2.id_user_role")->get()->result();
	}

	public function r_delivery_order_by_no_do_where_status_dalam_perjalanan($no_do){
		$this->db->from('delivery_order');
		$this->db->where('no_do', $no_do);
		return $this->db->where('status', 2)->get()->row();
	}

	public function u_status_delivery_order($no_do){
		$data = array(
        	'status' => 2,
		);
		$this->db->where('no_do', $no_do);
		return $this->db->update('delivery_order', $data);
	}

	public function u_status_emergency_delivery_order($no_do){
		$data = array(
        	'is_emergency' => true,
		);
		$this->db->where('no_do', $no_do);
		return $this->db->update('delivery_order', $data);
	}

	public function order_report_quantity(){
		
		return $this->db->query("SELECT status, count(status) CASE WHEN count(status) = null THEN 0 END test FROM public.delivery_order WHERE extract(month from created_date) = 11 GROUP BY status")->result_array();
	}
}
?>