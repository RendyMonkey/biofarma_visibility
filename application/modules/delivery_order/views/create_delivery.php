<link href="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/moment/moment.js') ?>"></script>
<script src="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.js') ?>"></script>
<script type="text/javascript" src="https://unpkg.com/@zxing/library@latest"></script>

<link href="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet"
    type="text/css">
<link href="<?= base_url('assets/plugins/datatables/buttons.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css">
<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.js') ?>"></script>
<style type="text/css">
    .myStyle{
        filter: blur(3px);
  -webkit-filter: blur(3px);
    }
body{ 
    width:100%;
   height:100%;
   margin:0;
   background-color:#fff;

}

.load{
  z-index: 999;
  position:absolute;
  max-width:100px;
  margin:0 auto;
  top: 50%;
  left:50%;
  transform: translate(-50%, -50%);
}



/*loading screen*/



.loading-screen{
  float:left;
  height:20px;
  width: 20px;
  margin:0 5px;
  border-radius:50%;
  animation: shrink 1s ease infinite 0ms;
  transform: scale(0.35);
}



/* animation */


.loading-screen:nth-child(1){
  animation: shrink 1s ease infinite 350ms;
  background-color:#45aaf2;
}

.loading-screen:nth-child(2){
  animation: shrink 1s ease infinite 550ms;
  background-color:#ffb8b8;
}

.loading-screen:nth-child(3){
  animation: shrink 1s ease infinite 700ms; 
  background-color:#f9ca24;
}



@keyframes shrink{
  50%{
    -webkit-transform: scale(1);
            transform: scale(1);
        opacity: 1;
  }
  
100%{
  opacity: 0;
}


}

.hidden{
    display: none;
}
</style>
<div class="load hidden">
  <div class="loading-screen"></div>
  <div class="loading-screen"></div>
  <div class="loading-screen"></div>
</div>
<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Receive Order</a></li>
                                <li class="breadcrumb-item active">Detail</li>
                            </ol>
                        </div>
                        <h4 class="page-title"><?= $this->session->userdata('nm_role_lv_2');?></h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <h5 class="text-primary">Create order</h5>

                            <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <th scope="row">No Delivery Order</th>
                                                <td> <input type="text" class="form-control" id="no_do"
                                                        placeholder="example : 0197123678123"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">No Request</th>
                                                <td> <input type="text" class="form-control" id="no_request"
                                                        placeholder=""></td>
                                            </tr>

                                            <tr>
                                                <th scope="row">Ship To</th>
                                                <td> <select class="form-control" id="receiver">
                                                        <option value="choose">choose...</option>
                                                    </select></td>
                                            </tr>

                                            <tr>
                                                <th scope="row">Delivery Date</th>
                                                <td> <input type="text" id="date-format" class="form-control"
                                                        placeholder="Saturday 24 June 2017"></td>
                                            </tr>

                                            <tr>
                                                <th scope="row">ETA</th>
                                                <td> <input type="text" class="form-control" placeholder="Jam"
                                                        id="eta_hours"></td>
                                            </tr>

                                            <tr>
                                                <th scope="row">UOM</th>
                                                <td> <select class="form-control" id="uom">
                                                        <option value="choose">choose...</option>
                                                        <option value="1">MasterBox</option>
                                                        <option value="2">InnerBox</option>
                                                        <option value="3">Vial</option>
                                                    </select></td>
                                            </tr>

                                            <tr>
                                                <th scope="row">Parent Delivery Order</th>
                                                <td> <select class="form-control" id="parent_do">
                                                        <option value="choose">choose...</option>
                                                    </select></td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <video id="webcam-preview" class="col"></video>
                                <center><button class="btn btn-primary self-align-center col-6" id="scanner" onclick="startScan()">Start Scan</button></center>
                                </div>
                            </div>
                            <table id="datatable" class="table table-bordered dt-responsive nowrap"
                                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr class="text-left">
                                        <th>#</th>
                                        <th>No Do</th>
                                        <th>GsOneId</th>
                                        <th>UOM</th>
                                        <th>Status Receive</th>
                                        <th>Product Model</th>
                                        <th>Product Name</th>
                                        <th>gs one inner box</th>
                                        <th>gs one master box</th>
                                        <th>Qty inner box</th>
                                        <th>Qty Vial</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <button type="button" id="submit" class="btn btn-primary">Submit</button>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container -->



        <footer class="footer text-center text-sm-left">&copy; 2020 Biotracking</footer>
        <!--end footer-->
    </div><!-- end page content -->
</div><!-- end page-wrapper -->

<script>
var table = $('#datatable').DataTable({
    'scrollX': true,
});

let count = 0;
let arrayGsOneId = [];
let haveScanned = false;
function startScan() {
    const codeReader = new ZXing.BrowserMultiFormatReader();
    codeReader.decodeFromVideoDevice(null, 'webcam-preview', (result, err) => {
        if (result) {
            var ketersediaan = false;
            $.ajax({
                url: '<?= base_url('delivery_order/getDetailDeliveryOrderByGsOneId/') ?>' + result,
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    if(data != null){
                        ketersediaan = true;
                    }
                }
            });

            var scanned = arrayGsOneId.find(function check(gsOneId) {
                 return gsOneId == result;
            });
            $.ajax({
                url: '<?= base_url('delivery_order/getDetailDeliveryOrderByGsOneId/') ?>' + result,
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    document.querySelector(".load").classList.add("hidden");
                    document.querySelector(".page-wrapper").classList.remove("myStyle");
                    if(data == null){
                        Swal.fire({
                            icon: 'warning',
                            title: 'Item not found !',
                        });
                    }else if(scanned != undefined){
                        Swal.fire({
                            icon: 'warning',
                            title: 'Item is already insert !',
                        });
                    }else{
                        arrayGsOneId.push(data.gs_on_id);
                        count += 1;
                        Swal.fire({
                            icon: 'success',
                            title: 'Success!',
                            html: '<p class="h4">Your data with GSOneID '+ result + ' is successfully scanned.</p>'
                        });
                        table.row.add([count,'<input class="form-control" type="text" name="no_do[]" value="'+data.no_do+'">', '<input class="form-control" type="text" name="gsOneId[]" value="'+data.gs_on_id+'">',  '<input class="form-control" type="text" name="uom[]" value="'+data.uom+'">', '<input class="form-control" type="text" name="status_received[]" value="'+data.status_received+'">', '<input class="form-control" type="text" name="product_model_id[]" value="'+data.product_model_id+'">', '<input class="form-control" type="text" name="product_name[]" value="'+data.product_name+'">', '<input class="form-control" type="text" name="gs_one_inner_box[]" value="'+data.gs_one_inner_box+'">', '<input class="form-control" type="text" name="gs_one_master_box[]" value="'+data.gs_one_master_box+'">', '<input class="form-control" type="text" name="qty_inner_box[]" value="'+data.qty_inner_box+'">', '<input class="form-control" type="text" name="qty_vial[]" value="'+data.qty_vial+'">']).draw();
                    }
                },
                error: function () {
                        document.querySelector(".load").classList.add("hidden");
                        document.querySelector(".page-wrapper").classList.remove("myStyle");
                        Swal.fire(
                            'Input Failed',
                            'Please contact developer to fix it.',
                            'error'
                        )
                    }
            });
            // properly decoded qr code
            // console.log('Found QR code!', result)
        codeReader.reset();
        $('#scanner').show();
    }
});

$('#scanner').hide();
}
 
</script>

<script>
    $('#date-format').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY',
        time: false,
    });
    //variable
    var urlGetUser = '<?= base_url('/user/User_account/getUserAccountByRoleLevel2Id/') ?>';

    $.ajax({
        url: '<?= base_url('delivery_order/getDeliveryOrderWithoutDatatable') ?>',
        type: 'get',
        dataType: 'json',
        success: function (data) {
            console.log(data);
            var html = '';
            $.each(data, function (key, dataValue) {
                html = '<option value="' + dataValue.no_do + '">' + dataValue.no_do + '</option>'
                $("#parent_do").append(html);
            });
        }
    });

    $.ajax({
        url: '<?= base_url('/user/User_role_level_2/getUserRoleLevel2AllWithoutDatatables') ?>',
        type: 'get',
        dataType: 'json',
        success: function (data) {
            console.log(data);
            var html = '';
            $.each(data, function (key, dataValue) {
                html = '<option value="' + dataValue.id_user_role + '">' + dataValue
                    .nm_role_lvl_2 + '</option>'
                $("#delivery").append(html);
                $("#receiver").append(html);
            });
        }
    });

    $('#delivery').on('change', function () {
        var html = '<option value="">Choose...</option>';
        $("#user_delivery").html(html);
        $.ajax({
            url: urlGetUser + this.value,
            type: 'get',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, dataValue) {
                    html = '<option value="' + dataValue.id_user_account + '">' + dataValue
                        .nm_lengkap + '</option>'
                    $("#user_delivery").append(html);
                });
            }
        });
    });

    $('#receiver').on('change', function () {
        var html = '<option value="">Choose...</option>';
        $("#user_receiver").html(html);
        $.ajax({
            url: urlGetUser + this.value,
            type: 'get',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, dataValue) {
                    html = '<option value="' + dataValue.id_user_account + '">' + dataValue
                        .nm_lengkap + '</option>'
                    $("#user_receiver").append(html);
                });
            }
        });
    });
    // POST TO DATABASE
    $(document).ready(function () {
        var values = [];
        $("#submit").click(function () {
            if(($('#receiver').val() == 'choose') || ($('#date-format').val() == '') || ($('#parent_do').val() == 'choose') || ($('#eta_hours').val() == '')){
                if($('#receiver').val() == 'choose'){
                    document.getElementById('receiver').focus();
                    var message = "Ship to is required";
                }else if($('#date-format').val() == ''){
                    var message = "Delivery date is required";
                }else if($('parent_do').val() == 'choose'){
                    var message = "Parent Delivery is required";
                }else{
                    var message = "ETA is required"
                }
            Swal.fire(
                'Input Warning !',
                message,
                'warning'
            );
        }else{
            for(var i = 0; i < document.getElementsByName("gsOneId[]").length; i++) {
                var object = {
                    no_do: document.getElementsByName("no_do[]")[i].value,
                    gsOneId : document.getElementsByName("gsOneId[]")[i].value,
                    uom : document.getElementsByName("uom[]")[i].value,
                    product_model_id : document.getElementsByName("product_model_id[]")[i].value,
                    product_name : document.getElementsByName("product_name[]")[i].value,
                    gs_one_inner_box : document.getElementsByName("gs_one_inner_box[]")[i].value,
                    gs_one_master_box : document.getElementsByName("gs_one_master_box[]")[i].value,
                    qty_inner_box : document.getElementsByName("qty_inner_box[]")[i].value,
                    qty_vial : document.getElementsByName("qty_vial[]")[i].value,
                    status_received : document.getElementsByName("status_received[]")[i].value,
                };
            values.push(object);
            }

            console.log(values);

            var data = {
                <?= $this->security->get_csrf_token_name(); ?> : '<?=$this->security->get_csrf_hash();?>',
                no_delivery : $("#no_do").val(),
                no_request: $("#no_request").val(),
                tanggal_pengiriman: $("#date-format").val(),
                id_role_receive: parseInt($("#receiver").val()),
                ETA: $("#eta_hours").val(),
                qty_delivery: $("#quantity").val(),
                parent_do: $("#parent_do").val(),
                detail_order : values
            };

            $.ajax({
                url: '<?= base_url('delivery_order/createDeliveryWithDetailOrder') ?>',
                type: 'post',
                dataType: 'json',
                data: data,
                success: function (response) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Success!',
                        html: '<p class="h4">Your data is successfully input.</p>' +
                            '<a class="card-lin" href="<?= base_url('delivery_order/DeliveryOrder/list') ?>">Go to list page!</a>'
                    });


                    $("#date-format").val('');
                    $("#eta_hours").val('');
                    $("#quantity").val('');
                    $("#receiver").val('');
                    $("#no_do").val('');
                    $("#no_request").val('');
                    $("#parent_do").val('choose');
                    values = [];
                },
                error: function () {
                    values = [];
                    alert('gagal');
                }
            });
            }
        });
    });
</script>