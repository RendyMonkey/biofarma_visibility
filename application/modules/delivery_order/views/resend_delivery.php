<link href="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/moment/moment.js') ?>"></script>
<script src="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.js') ?>"></script>

<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Delivery Order</a></li>
                                <li class="breadcrumb-item active">Resend</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Resend Delivery Order</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form class=" col-8 offset-2">
                                <div class="form-group">
                                    <label for="category">No Delivery Order</label>
                                    <input type="text" class="form-control" id="no_do" placeholder="example : 0197123678123" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="category">No Request</label>
                                    <input type="text" class="form-control" id="no_request" disabled>
                                </div>

                                <div class="form-group">
                                    <label for="category">Ship to</label>
                                    <select class="form-control" id="receiver" disabled>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="time">Delivery Date</label>
                                    <input type="text" id="date-format" class="form-control"
                                        placeholder="Saturday 24 June 2017" disabled>
                                </div>

                                <div class="form-group">
                                    <label for="category">ETA</label>
                                    <div class="row">
                                        <div class="col">
                                            <input type="text" class="form-control" placeholder="Jam" id="eta_hours">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="category">UOM</label>
                                    <select class="form-control" id="uom" disabled>
                                        <option value="1">MasterBox</option>
                                        <option value="2">InnerBox</option>
                                        <option value="3">Vial</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="category">Quantity</label>
                                    <input type="text" class="form-control" id="quantity" disabled>
                                </div>

                                <div class="form-group">
                                    <label for="category">Parent DO</label>
                                    <select class="form-control" id="parent_do" disabled>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="category">Status</label>
                                    <select class="form-control" id="status" disabled>
                                        <option value="1">Dalam Perjalanan</option>
                                        <option value="2">Sedang Dalam Penerimaan</option>
                                        <option value="3">Sudah Diteriman</option>
                                    </select>
                                </div>
                    </div>
                    <button type="button" id="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div><!-- end col -->
    </div><!-- end row -->
</div><!-- container -->
</div><!-- end page content -->
</div><!-- end page-wrapper -->

<script>

    // POST TO DATABASE
    $(document).ready(function () {
        $.ajax({
            url: '<?= base_url('delivery_order/getDeliveryOrderWithoutDatatable') ?>',
            type: 'get',
            dataType: 'json',
            success: function(data){
                console.log(data);
                var html = '';
                $.each(data, function(key, dataValue){
                    html = '<option value="' + dataValue.do_id + '">' + dataValue.no_do + '</option>'
                    $("#parent_do").append(html);
                });
            }
        });

        $.ajax({
        url: '<?= base_url('/user/User_role_level_2/getUserRoleLevel2AllWithoutDatatables') ?>',
        type: 'get',
        dataType: 'json',
        success: function (data) {
            var html = '';
            $.each(data, function (key, dataValue) {
                html = '<option value="' + dataValue.id_user_role + '">' + dataValue
                    .nm_role_lvl_2 + '</option>';
                $("#receiver").append(html);
            });
        }
    }),

        $.ajax({
        url: '<?= base_url('delivery_order/getDeliveryOrderByIdJoinUserLevel2/').$id;?>',
        type: 'get',
        dataType: 'json',
        success: function (data) {

            $("#no_do").val(data.no_do);
            $("#no_request").val(data.no_request);
            document.getElementById('receiver').value = data.id_role_receive;
            $("#eta_hours").val(data.eta);
            $("#quantity").val(data.qty_delivery);
            $("#uom").val(data.UOM);
            $("#status").val(data.status);
            $("#date-format").val(moment(data.tanggal_pengiriman).format("dddd DD MMMM YYYY"));
            $("#parent_do").val(data.parent_do);
        }
    }),

        $("#submit").click(function () {
            var data = {
                <?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>', 
                ETA: $("#eta_hours").val()
            };
            console.log(data);
            $.ajax({
                url: '<?= base_url('delivery_order/resendDelivery/').$id ?>',
                type: 'post',
                dataType: 'json',
                data: data,
                success: function (response) {

                    Swal.fire({
                        icon: 'success',
                        title: 'Success!',
                        html: '<p class="h4">Your Delivery has been Resend.</p>'
                    });
                },
                error: function () {
                    alert('gagal');
                }
            });
        });
    });
</script>