<link href="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/moment/moment.js') ?>"></script>
<script src="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.js') ?>"></script>
<script type="text/javascript" src="https://unpkg.com/@zxing/library@latest"></script>

<link href="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet"
    type="text/css">
<link href="<?= base_url('assets/plugins/datatables/buttons.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css">
<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.js') ?>"></script>
<style type="text/css">
    .myStyle{
        filter: blur(3px);
  -webkit-filter: blur(3px);
    }
body{ 
    width:100%;
   height:100%;
   margin:0;
   background-color:#fff;

}

.load{
  z-index: 999;
  position:absolute;
  max-width:100px;
  margin:0 auto;
  top: 50%;
  left:50%;
  transform: translate(-50%, -50%);
}



/*loading screen*/



.loading-screen{
  float:left;
  height:20px;
  width: 20px;
  margin:0 5px;
  border-radius:50%;
  animation: shrink 1s ease infinite 0ms;
  transform: scale(0.35);
}



/* animation */


.loading-screen:nth-child(1){
  animation: shrink 1s ease infinite 350ms;
  background-color:#45aaf2;
}

.loading-screen:nth-child(2){
  animation: shrink 1s ease infinite 550ms;
  background-color:#ffb8b8;
}

.loading-screen:nth-child(3){
  animation: shrink 1s ease infinite 700ms; 
  background-color:#f9ca24;
}



@keyframes shrink{
  50%{
    -webkit-transform: scale(1);
            transform: scale(1);
        opacity: 1;
  }
  
100%{
  opacity: 0;
}


}

.hidden{
    display: none;
}
</style>
<div class="load hidden">
  <div class="loading-screen"></div>
  <div class="loading-screen"></div>
  <div class="loading-screen"></div>
</div>
<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Receive Order</a></li>
                                <li class="breadcrumb-item active">Detail</li>
                            </ol>
                        </div>
                        <h4 class="page-title"><?= $this->session->userdata('nm_role_lv_2');?></h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <h5 class="text-primary">Scan order</h5>

                            <div class="row" style="width: 300px;">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <video id="webcam-preview" class="col"></video>
                                <center><button class="btn btn-primary self-align-center col-6" id="scanner" onclick="startScan()">Start Scan</button></center>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <button type="button" id="submit" class="btn btn-primary">Submit</button> -->
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container -->



        <footer class="footer text-center text-sm-left">&copy; 2020 Biotracking</footer>
        <!--end footer-->
    </div><!-- end page content -->
</div><!-- end page-wrapper -->

<script>

function startScan() {
    const codeReader = new ZXing.BrowserMultiFormatReader();
    codeReader.decodeFromVideoDevice(null, 'webcam-preview', (result, err) => {
        if (result) {
            var ketersediaan = false;
            $.ajax({
                url: '<?= base_url('delivery_order/getDetailDeliveryOrderByNoDoWhereStatusDalamPerjalanan/') ?>' + result,
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    if(data != null){
                        ketersediaan = true;
                    }

                    if(ketersediaan){
                        Swal.fire({
                        icon: 'warning',
                        title: 'warning!',
                        html: '<p class="h4">Data already scan.</p>'
                });
            }else{

            $.ajax({
                url: '<?= base_url('delivery_order/scanDeliveryOrder') ?>',
                type: 'post',
                dataType: 'json',
                data: {
                    <?= $this->security->get_csrf_token_name(); ?> : '<?=$this->security->get_csrf_hash();?>',
                    no_do : result
                },
                success: function (response) {
                    document.querySelector(".load").classList.add("hidden");
                    document.querySelector(".page-wrapper").classList.remove("myStyle");
                    Swal.fire({
                        icon: 'success',
                        title: 'Success!',
                        html: '<p class="h4">Scan data is successfully.</p>'
                    });
                },
                error: function () {
                    document.querySelector(".load").classList.add("hidden");
                    document.querySelector(".page-wrapper").classList.remove("myStyle");
                    alert('gagal');
                }
            });   
            }
                }
            });
            
            
            // properly decoded qr code
            // console.log('Found QR code!', result)
        codeReader.reset();
        $('#scanner').show();
    }
});

$('#scanner').hide();
}
 
</script>

