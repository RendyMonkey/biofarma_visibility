<link href="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet"
  type="text/css">
<link href="<?= base_url('assets/plugins/datatables/buttons.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css">
<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.js') ?>"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<style type="text/css">
    .myStyle{
        filter: blur(3px);
  -webkit-filter: blur(3px);
    }
  body{ 
      width:100%;
    height:100%;
    margin:0;
    background-color:#fff;

  }

  .load{
    z-index: 999;
    position:absolute;
    max-width:100px;
    margin:0 auto;
    top: 50%;
    left:50%;
    transform: translate(-50%, -50%);
  }



  /*loading screen*/



  .loading-screen{
    float:left;
    height:20px;
    width: 20px;
    margin:0 5px;
    border-radius:50%;
    animation: shrink 1s ease infinite 0ms;
    transform: scale(0.35);
  }



  /* animation */


  .loading-screen:nth-child(1){
    animation: shrink 1s ease infinite 350ms;
    background-color:#45aaf2;
  }

  .loading-screen:nth-child(2){
    animation: shrink 1s ease infinite 550ms;
    background-color:#ffb8b8;
  }

  .loading-screen:nth-child(3){
    animation: shrink 1s ease infinite 700ms; 
    background-color:#f9ca24;
  }



  @keyframes shrink{
    50%{
      -webkit-transform: scale(1);
              transform: scale(1);
          opacity: 1;
    }
    
  100%{
    opacity: 0;
  }


  }

  .hidden{
      display: none;
  }
</style>
<div class="load hidden">
  <div class="loading-screen"></div>
  <div class="loading-screen"></div>
  <div class="loading-screen"></div>
</div>
<div class="page-wrapper">
  <!-- Page Content-->
  <div class="page-content-tab">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <div class="page-title-box">
            <div class="float-right">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0);">Report</a></li>
                <li class="breadcrumb-item active">GsOneId</li>
              </ol>
            </div>
            <h4 class="page-title">List Trackback GsOneId</h4>
          </div>
          <!--end page-title-box-->
        </div>
        <!--end col-->
      </div><!-- end page title end breadcrumb -->

      <div class="row">
        <div class="col-3 mb-3">
         <select class="form-control theSelect" id="GsOneId" onchange="searchGsOneId()">
              <option value="">Choose...</option>
          </select>
        </div>
        <div class="col-3">
        </div>
      </div>

      <div class="row">
      <div class="col-12">
          <div class="card">
            <div class="card-body">
              <table id="datatable" class="table table-bordered nowrap"
                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>GSOneID</th>
                    <th>UOM</th>
                  </tr>
                </thead>
                <tbody id="data">
                </tbody>
              </table>
            </div>
          </div>
        </div><!-- end col -->
      </div><!-- end row -->
    </div><!-- container -->

    <!--  Modal content for the above example -->
    <footer class="footer text-center text-sm-left">&copy; 2020 Biotracking <span
        class="text-muted d-none d-sm-inline-block float-right"></i>
        by Mannatthemes</span></footer>
    <!--end footer-->
  </div><!-- end page content -->
</div><!-- end page-wrapper -->

<script>
    $(".theSelect").select2();
    $.ajax({
        url: '<?= base_url('delivery_order/getDeliveryOrderWithoutDatatable') ?>',
        type: 'get',
        dataType: 'json',
        success: function (data) {
            console.log(data);
            var html = '';
            $.each(data, function (key, dataValue) {
                html = '<option value="' + dataValue.no_do + '">' + dataValue.no_do + '</option>'
                $("#parent_do").append(html);
            });
        }
    });

    function searchGsOneId(){
      table.clear().draw();
      document.querySelector(".load").classList.remove("hidden");
      document.querySelector(".page-wrapper").classList.add("myStyle");
      let no_do = document.getElementById('parent_do').value;
      let count = 1;
       $.ajax({
        url: '<?= base_url('delivery_order/getTrackBackDeliveryByParentDo') ?>',
        type: 'post',
        dataType: 'json',
        data: {
          <?= $this->security->get_csrf_token_name(); ?> : '<?=$this->security->get_csrf_hash();?>',
          no_do : no_do
        },
        success: function (data) {
            document.querySelector(".load").classList.add("hidden");
            document.querySelector(".page-wrapper").classList.remove("myStyle");
            var html = '';
            $.each(data, function (key, dataValue) {
                table.row.add([count++, dataValue.no_do, dataValue.nm_role_lvl_2, dataValue.tanggal_pengiriman]).draw();
            });
        },
        error: function () {
                        document.querySelector(".load").classList.add("hidden");
                        document.querySelector(".page-wrapper").classList.remove("myStyle");
                        Swal.fire(
                            'Input Failed',
                            'Please contact developer to fix it.',
                            'error'
                        )
                    }
    });
    }
    var table = $('#datatable').DataTable();
</script>