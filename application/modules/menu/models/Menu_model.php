<?php 
class Menu_model extends CI_Model{

	public function c_menu($data){
		$query = $this->db->insert('menu', $data);
		return $query;
	}

	public function r_menu_query(){
		$search_attribut = ["nama_menu", "category"];
		$column_order = array(null, "nama_menu", "category");
		search_datatable("menu", $search_attribut, $column_order);
	}

	public function r_menu(){
		$this->r_menu_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	public function r_menu_count_all()
    {
        $this->db->from('menu');
        return $this->db->count_all_results();
	}
	
	function r_menu_count_filtered()
    {
        $this->r_menu_query();
        $query = $this->db->get();
        return $query->num_rows();
	}

	public function r_menu_by_id($id){
		$query = $this->db->get_where('menu',['id_menu' => $id]);
		return $query->row();
	}

	public function u_menu($id, $data){
		$this->db->where('id_menu', $id);
		$query = $this->db->update('menu', $data);
		return $query;
	}

	public function d_menu($id){
		$query = $this->db->delete('menu', array('id_menu' => $id));
		return $query;
	}

	public function r_menu_all(){
		$query = $this->db->get('menu');
		return $query->result();
	}

	public function r_menu_where_type($type){
		$this->db->from('menu');
		return $this->db->where('category', $type)->get()->result();
	}

	public function r_menu_where_not_byId($id, $type){
		$this->db->from('menu');
		$this->db->where('type', $type);
		return $this->db->where_not_in('id_menu', $id)->get()->result();
	}
}
?>