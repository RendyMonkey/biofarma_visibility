<link href="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/moment/moment.js') ?>"></script>
<script src="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.js') ?>"></script>
<link href="<?= base_url('assets/plugins/dropify/dropify.min.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/dropify/dropify.min.js') ?>"></script>

<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= base_url() ?>">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">User</a></li>
                                    <li class="breadcrumb-item active">Update user account</li>

                            </ol>
                        </div>
                        <h4 class="page-title">Update user account</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form class=" col-8 offset-2">
                                <div class="form-group">
                                    <label for="category">Instansi</label>
                                    <select class="form-control" id="id_user_role_level_2">
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="category">IMEI</label>
                                    <input type="text" class="form-control" id="imei">
                                </div>

                                <div class="form-group">
                                    <label for="category">Username</label>
                                    <input type="text" class="form-control" id="username">
                                </div>

                                <!-- <div class="form-group">
                                    <label for="category">Password</label>
                                    <input type="text" class="form-control" id="password">
                                </div> -->

                                <div class="form-group">
                                    <label for="category">Full Name</label>
                                    <input type="text" class="form-control" id="nm_lengkap">
                                </div>

                                <div class="form-group">
                                    <label for="category">Email</label>
                                    <input type="text" class="form-control" id="user_account_email">
                                </div>

                                <div class="form-group">
                                    <label for="category">No Telpone</label>
                                    <input type="text" class="form-control" id="no_telphone">
                                </div>

                                <div class="form-group">
                                    <label for="category">Active</label>
                                    <select class="form-control" id="isActive">
                                    <option value="t">True</option>
                                    <option value="f">False</option>
                                    </select>
                                </div>
                                </div>
                        </div>
                        <button type="button" id="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- container -->
</div><!-- end page content -->
</div><!-- end page-wrapper -->

<script>
$.ajax({
            url: '<?= base_url('/user/User_role_level_2/getUserRoleLevel2AllWithoutDatatables') ?>',
            type: 'get',
            dataType: 'json',
            success: function(data){
                console.log(data);
                var html = '';
                $.each(data, function(key, dataValue){
                    html = '<option value="' + dataValue.id_user_role + '">' + dataValue.nm_role_lvl_2 + '</option>'
                    $("#id_user_role_level_2").append(html);
                });
            }
    });
    
$.ajax({
            url: '<?= site_url('user/User_account/getUserAccountById/').$id?>',
            type: 'get',
            dataType: 'json',
            success: function(data){
                console.log(data.id_role_level_2);
                $('#id_user_role_level_2').val(data.id_role_level_2);
                $('#imei').val(data.imei);
                $('#username').val(data.username);
                $('#nm_lengkap').val(data.nm_lengkap);
                $('#user_account_email').val(data.user_account_email);
                $('#no_telphone').val(data.no_telephone);
                // $('#isActive').val(data.isActive);
            }
        });

$('#submit').click( function(){
    if(($('#id_user_role_level_2').val() == 'choose') || ($('#imei').val().length < 1) || ($('#nm_lengkap').val() < 1) || ($('#user_account_email').val() < 1) || ($('#no_telphone').val() < 1)){
          if($('#id_user_role_level_2').val() == 'choose'){
            var message = "Instansi is required";
          }else if($('#imei').val().length < 1){
            document.getElementById('imei').focus();
            var message = "Imei is required";
          }else if($('#nm_lengkap').val().length < 1){
            document.getElementById('nm_lengkap').focus();
            var message = "Name required";
          }else if($('#user_account_email').val().length < 1){
            document.getElementById('user_account_email').focus();
            var message = "Email is required";
          }else{
            document.getElementById('no_telphone').focus();
            var message = "Telephone is required";
          }
          Swal.fire(
              'Input Warning !',
              message,
              'warning'
          );
    }else{
        $.ajax({
            url: '<?= base_url('user/User_account/updateUserAccount/').$id ?>',
            type: 'post',
            dataType: 'json',
            data: {
                <?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>', 
                id_role_level_2: $('#id_user_role_level_2').val(),
                imei: $('#imei').val(),
                username: $('#username').val(),
                password: $('#password').val(),
                nm_lengkap: $('#nm_lengkap').val(),
                user_account_email: $('#user_account_email').val(),
                no_telphone: $('#no_telphone').val(),
                isActive: $('#isActive').val(),
            },
            success: function (data) {
                Swal.fire(
                'Data has been created!',
                'Your data is successfuly created.',
                'success'
                );
            }, error: function(){
                Swal.fire(
                'Data not created!',
                'Please contact developer to fix it.',
                'error'
                )
            }
        })
    }
       });
</script>