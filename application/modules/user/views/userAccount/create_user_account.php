<link href="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/moment/moment.js') ?>"></script>
<script src="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.js') ?>"></script>
<link href="<?= base_url('assets/plugins/dropify/dropify.min.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/dropify/dropify.min.js') ?>"></script>

<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= base_url() ?>">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">User</a></li>
                                    <li class="breadcrumb-item active">List User Account</li>

                            </ol>
                        </div>
                        <h4 class="page-title">Create Permission</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form class=" col-8 offset-2">

                                <div class="form-group">
                                    <label for="category">Instansi</label>
                                    <select class="form-control" id="id_user_role_level_2">
                                        <option value="choose">Choose...</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="category">Full Name</label>
                                    <input type="text" class="form-control" id="nm_lengkap">
                                </div>

                                <div class="form-group">
                                    <label for="category">Email</label>
                                    <input type="text" class="form-control" id="user_account_email">
                                </div>

                                <div class="form-group">
                                    <label for="category">No Telpone</label>
                                    <input type="number" class="form-control" id="no_telphone">
                                </div>
                                </div>
                        </div>
                        <button type="button" id="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- container -->
</div><!-- end page content -->
</div><!-- end page-wrapper -->

<script>
    $.ajax({
            url: '<?= base_url('/user/User_role_level_2/getUserRoleLevel2AllWithoutDatatables') ?>',
            type: 'get',
            dataType: 'json',
            success: function(data){
                console.log(data);
                var html = '';
                $.each(data, function(key, dataValue){
                    html = '<option value="' + dataValue.id_user_role + '">' + dataValue.nm_role_lvl_2 + '</option>'
                    $("#id_user_role_level_2").append(html);
                });
            }
    });
    
    $('#submit').click( function(){
        if(($('#id_user_role_level_2').val() == 'choose') || ($('#nm_lengkap').val() < 1) || ($('#user_account_email').val() < 1) || ($('#no_telphone').val() < 1)){
          if($('#id_user_role_level_2').val() == 'choose'){
            var message = "Instansi is required";
          }else if($('#nm_lengkap').val().length < 1){
            document.getElementById('nm_lengkap').focus();
            var message = "Name required";
          }else if($('#user_account_email').val().length < 1){
            document.getElementById('user_account_email').focus();
            var message = "Email is required";
          }else{
            document.getElementById('no_telphone').focus();
            var message = "Telephone is required";
          }
          Swal.fire(
              'Input Warning !',
              message,
              'warning'
          );
    }else{
        $.ajax({
            url: '<?= base_url('user/User_account/createUserAccount') ?>',
            type: 'post',
            dataType: 'json',
            data: {
                <?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>', 
                id_user_role_level_2: $('#id_user_role_level_2').val(),
                username: $('#username').val(),
                password: $('#password').val(),
                nm_lengkap: $('#nm_lengkap').val(),
                user_account_email: $('#user_account_email').val(),
                no_telphone: $('#no_telphone').val(),
                // isActive: $('#isActive').val(),
            },
            success: function (data) {
                Swal.fire(
                'Data has been created!',
                'Your data is successfuly created.',
                'success'
                );

                $('#id_user_role_level_2').val(''),
                $('#username').val(''),
                $('#password').val(''),
                $('#nm_lengkap').val(''),
                $('#user_account_email').val(''),
                $('#no_telphone').val('')
                // $('#isActive').val('')

            }, error: function(){
                Swal.fire(
                'Data not created!',
                'Please contact developer to fix it.',
                'error'
                )
            }
        })
    }
       });
</script>