s<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class User_account extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 		$this->load->model('User_account_model');
 		if(!$this->session->has_userdata('nama')){
 			redirect(base_url("login"));
 		}
 	}

 	public function createUserAccount()
 	{
 		$password = uniqid(10);
 		$data = [
			"id_user_account" => rand(100,10000),
			"id_role_level_2" => $this->input->post('id_user_role_level_2'),
 			"username" => uniqid(10),
 			"password" => password_hash($password, PASSWORD_DEFAULT),
 			"nm_lengkap" => $this->input->post('nm_lengkap'),
 			"user_account_email" => $this->input->post('user_account_email'),
 			"no_telephone" => $this->input->post('no_telphone'),
			"created_by" => $this->session->userdata('id_user_account'),
			"created_date" => date("Y-m-d H:i:s"),
			"activation_code" => rand()
			 
 		];
 		$input = $this->User_account_model->c_user_account($this->security->xss_clean($data));
 		if($input){
 			log_record(null, json_encode($data));
			 $data = [
				'email_penerima' => $data['user_account_email'],
				'subject' => 'Generate Account from Biofarma Admin',
				'body' => '<h3>Data Account</h3> 
				<p>Username = '.$data["username"]. '</p>
				<p>Password = '.$password.'</p>
				<p>Activation Code = '.$data['activation_code'].'</p>'
			];
			echo json_encode(['message' => 'input data berhasil']);
			$this->load->library('mailer');
			$send = $this->mailer->send($data);
 		}else{
 			echo json_encode(['message' => 'input data gagal']);
 		}
 	}

 	public function getUserAccount()
 	{
		$list = $this->User_account_model->r_user_account();
		$all_data = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$row = array();
			$no++;
			$row[] = $no;
			$row[] = $data->imei;
			$row[] = $data->username;
			$row[] = $data->nm_lengkap;
			$row[] = $data->no_telephone;
			$row[] = $data->user_account_email;
			$row[] = '<a href="'. base_url("user/User_account/update/").$data->id_user_account .'" class="mr-2">
					   	<i class="las la-pen text-info font-18"></i>
					   </a>
                    <a href="#" onclick="delete_data('.$data->id_user_account.')">
					 <i class="las la-trash-alt text-danger font-18"></i>
												</a>';

			$all_data[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->User_account_model->r_user_account_count_all(),
                        "recordsFiltered" => $this->User_account_model->r_user_account_count_filtered(),
                        "data" => $all_data,
		); 
 		echo json_encode($result);
	 }

	 public function getCountNewUserAccount(){
	 	echo json_encode($this->User_account_model->r_count_new_user_account());
	 }

	 public function getCountActiveUserAccount(){
	 	echo json_encode($this->User_account_model->r_count_user_account_active());
	 }

	 public function getCountAllUserAccount(){
	 	echo json_encode($this->User_account_model->r_user_account_count_all());
	 }

	 public function getCountAllUserAccountLogin(){
	 	echo json_encode($this->User_account_model->r_count_user_account_login());
	 }

	 public function getUserAccountByRoleLevel2Id($id){
		echo json_encode($this->User_account_model->r_user_account_by_role_level_2_id($id));
	 }

	 public function getUserAccountWithoutDatatables(){
		 echo json_encode($this->User_account_model->r_user_account_without_datatables());
	 }

	 public function getUserAccountById($id){
		echo json_encode($this->User_account_model->r_user_account_by_id($id));
	 }

 	public function updateUserAccount($id)
 	{
 		$data = [
			"id_role_level_2" => $this->input->post('id_role_level_2'),
 			"imei" => $this->input->post('imei'),
 			"username" => $this->input->post('username'),
 			"nm_lengkap" => $this->input->post('nm_lengkap'),
 			"user_account_email" => $this->input->post('user_account_email'),
 			"no_telephone" => $this->input->post('no_telphone'),
 			"is_active" => $this->input->post('isActive'),
			"updated_by" => $this->session->userdata('id_user_account'),
		   	"updated_date" => date("Y-m-d H:i:s")
 		];
 		log_record(json_encode($this->User_account_model->r_user_account_by_id($id)), $data);
 		$update = $this->User_account_model->u_user_account($id, $this->security->xss_clean($data));
 		if($update){
 			echo json_encode(['message' => 'update data berhasil']);
 		}else{
 			echo json_encode(['message' => 'update data gagal']);
 		}
 	}

 	public function deleteUserAccount($id)
 	{
 		log_record(json_encode($this->User_account_model->r_user_account_by_id($id)), null);
 		$delete = $this->User_account_model->d_user_account($id);
 		echo json_encode(['message' => 'delete data berhasil']);
	}
	 
	 
	//  FOR VIEW
	public function list()
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('userAccount/list_user_account');
	   $this->load->view('_rootComponents/_footer/footer');
	}

	public function detail()
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('userAccount/detail_user_account');
	   $this->load->view('_rootComponents/_footer/footer');
	}

	public function create()
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('userAccount/create_user_account');
	   $this->load->view('_rootComponents/_footer/footer');
	}

	public function update($id)
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('userAccount/update_user_account', ['id' => $id]);
	   $this->load->view('_rootComponents/_footer/footer');
	}
}