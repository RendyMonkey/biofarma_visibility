<?php 
class User_account_model extends CI_Model{

	public function c_user_account($data){
		$query = $this->db->insert('user_account', $data);
		return $query;
	}

	public function r_user_account_query(){		
		$search_attribut = ["imei", "username", "nm_lengkap", "no_telephone", "user_account_email"];
		$column_order = array(null, "imei", "username", "nm_lengkap", "no_telephone", "user_account_email");
		search_datatable("user_account", $search_attribut, $column_order);
	}

	public function r_user_account(){
		$this->r_user_account_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	public function r_count_new_user_account(){
		$this->db->from('user_account');
		$this->db->where('created_date >=', date('Y-m-d', mktime( 0, 0, 0, date('n') - 5, date('j'), date('y'))));
        return $this->db->count_all_results();
	} 

	public function r_count_deleted_user_account(){
		$this->db->from('user_account');
		$this->db->where('is_deleted >=', true);
        return $this->db->count_all_results();
	} 

	public function u_user_account($id, $data){
		$this->db->where('id_user_account', $id);
		$query = $this->db->update('user_account', $data);
		return $query;
	}

	public function d_user_account($id){
		$query = $this->db->delete('user_account', array('id_user_account' => $id));
		return $query;
	}

	public function r_imei_account_by_id($id)
	{
		return $this->db->get_where('user_account', ['id_user_account' => $id])->row();
	}

	public function r_user_account_by_role_level_2_id($id){
		return $this->db->get_where('user_account', ['id_role_level_2' => $id	])->result();
	}

	public function r_user_account_by_id($id){
		return $this->db->get_where('user_account', ['id_user_account' => $id])->row();
	}

	public function r_user_account_count_all()
    {
        $this->db->from('user_account');
        return $this->db->count_all_results();
	}

	public function r_user_account_count_all_by_id_role_level_2($id)
    {
		$this->db->from('user_account');
		$this->db->where('id_role_level_2', $id);
        return $this->db->count_all_results();
	}
	
	function r_user_account_count_filtered()
    {
        $this->r_user_account_query();
        $query = $this->db->get();
        return $query->num_rows();
	}

	public function r_count_user_account_active()
    {
        return $this->db->get_where('user_account', ['is_active' => true])->num_rows();   
	}

	public function r_count_user_account_active_by_id_role_level_2($id)
    {
        return $this->db->get_where('user_account', ['is_active' => true, 'id_role_level_2' => $id])->num_rows();   
	}

	public function r_count_user_account_active_by_id_role_lvl_2($id)
    {
        return $this->db->get_where('user_account', ['is_active' => true, 'id_role_level_2' => $id])->num_rows();   
	}

	public function r_count_user_account_login()
    {
        return $this->db->get_where('user_account', ['is_login' => true])->num_rows();   
	}

	public function r_count_user_account_login_by_id_role_level_2($id)
    {
        return $this->db->get_where('user_account', ['is_login' => true, 'id_role_level_2' => $id])->num_rows();   
	}

	public function r_user_account_without_datatables(){
		return $this->db->get('user_account')->result();
	}
}
?>