<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class recooling extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 		$this->load->helper('file');
 		$this->load->model('recooling_model');
 		if(!$this->session->has_userdata('nama')){
 			redirect(base_url("login"));
 		}
 	}

 	public function getRecooling()
 	{
		$startDate = $this->input->post('startDate');
		$endDate = $this->input->post('endDate');
		$list = $this->recooling_model->r_recooling($startDate, $endDate);
		$all_data = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$row = array();
			$no++;
			$row[] = $no;
			$row[] = $data->category;
			$row[] = $data->description;
			$row[] = $data->foto;
			$row[] = $data->time_recooling;
			$row[] = $data->username;
			$row[] = '<a href='. base_url("recooling/listDoRecooling/").$data->id_recooling.' class="mr-2">
                       <i class="las la-clipboard font-18"></i></a>';
			$all_data[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->recooling_model->r_recooling_count_all(),
                        "recordsFiltered" => $this->recooling_model->r_recooling_count_filtered($startDate, $endDate),
                        "data" => $all_data,
		); 
 		echo json_encode($result);
 	}

    public function getRecooling2(){
    	$startDate = $this->input->post('startDate');
		$endDate = $this->input->post('endDate');
		$list = $this->recooling_model->r_recooling($startDate, $endDate);
		$all_data = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$row = array();
			$no++;
			$row[] = $no;
			$row[] = $data->username;
			$row[] = $data->category;
			$row[] = $data->description;
			$row[] = $data->time_recooling;
			$all_data[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->recooling_model->r_recooling_count_all(),
                        "recordsFiltered" => $this->recooling_model->r_recooling_count_filtered($startDate, $endDate),
                        "data" => $all_data,
		); 
 		echo json_encode($result);
    }

 	public function getRecoolingById($id){
		echo json_encode($this->recooling_model->r_recooling_by_id($id));
	}

	public function exportReportRecooling(){
		$data = $this->recooling_model->r_reccoling_report();
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'No');
		$sheet->setCellValue('B1', 'Kategori Recooling');
		$sheet->setCellValue('C1', 'Description');
		$sheet->setCellValue('D1', 'Time Reccoling');
		$sheet->setCellValue('E1', 'User');
		$no = 1;
		$x = 2;
		foreach($data as $row)
		{
			$sheet->setCellValue('A'.$x, $no++);
			$sheet->setCellValue('B'.$x, $row->category);
			$sheet->setCellValue('C'.$x, $row->description);
			$sheet->setCellValue('D'.$x, $row->time_recooling);
			$sheet->setCellValue('E'.$x, $row->username);
			$x++;
		}
		$writer = new Xlsx($spreadsheet);
		$filename = 'Report_recooling';
			
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');
	
		$writer->save('php://output');
	}

	// FOR VIEW
	public function list()
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('list_recooling');
	   $this->load->view('_rootComponents/_footer/footer');
	}

	public function listDoRecooling($id)
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('list_do_recooling', ['id' => $id]);
	   $this->load->view('_rootComponents/_footer/footer');
	}

	public function detail($id)
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('detail_recooling', ['id' => $id]);
	   $this->load->view('_rootComponents/_footer/footer');
	}

	public function create()
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('create_recooling');
	   $this->load->view('_rootComponents/_footer/footer');
	}

	public function update($id)
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('update_recooling', ['id' => $id]);
	   $this->load->view('_rootComponents/_footer/footer');
	}
}