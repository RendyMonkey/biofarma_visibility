<link href="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/moment/moment.js') ?>"></script>
<script src="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.js') ?>"></script>
<link href="<?= base_url('assets/plugins/dropify/dropify.min.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/dropify/dropify.min.js') ?>"></script>

<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= base_url() ?>">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">recooling</a></li>
                                    <li class="breadcrumb-item active">Create recooling</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Create recooling</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form class=" col-8 offset-2"  enctype="multipart/form-data" id="frm">
                                <div class="form-group">
                                    <label for="category">recooling Category</label>
                                    <input type="text" class="form-control" name="category_recooling" id="category">
                                </div>
                                
                                <div class="form-group">
                                    <label for="category">ID SO</label>
                                    <input type="text" class="form-control" name="id_so" id="id_so" value="1">
                                </div>

                                <div class="form-group">
                                    <label for="category">User Account</label>
                                    <input type="text" class="form-control" name="id_user_account" id="user">
                                </div>

                                <div class="form-group">
                                    <label for="time">Time recooling</label>
                                    <input type="text" id="date-format" name="time_recooling" class="form-control"
                                        placeholder="Saturday 24 June 2017 - 21:44">
                                </div>

                                <div class="form-group">
                                    <label for="time">recooling Description</label>
                                    <textarea class="form-control" name="descriptionrecooling" id="description" rows="4"></textarea>
                                </div>

                                <div class="form-group" id="img_upload">
                                    <label for="photo">Photo</label>
                                </div>

                                <button type="button" id="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container -->
    </div><!-- end page content -->
</div><!-- end page-wrapper -->

<script>
    $('#date-format').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY',
        time:false
    });
    $('.dropify').dropify();
    
    $.ajax({
        url: '<?= base_url('recooling/recooling/getrecoolingWithDetailById/').$id ?>',
        type: 'get',
        dataType: 'json',
        success: function (data) {
            $("#category").val(data.category_recooling);
            $("#id_so").val(data.id_so);
            $("#user").val(data.id_user_account);
            $("#date-format").val(moment(data.time_recooling).format("dddd DD MMMM YYYY"));
            $("#description").val(data.descriptionrecooling);
            $('#img_upload').html(
                '<input type="file" name="foto_recooling" class="dropify"  data-show-remove="false" data-default-file="'+"<?= base_url('/assets/images/recooling/') ?>" + data.foto_recooling +'" id="foto" data-height="300">'
            );
            $('.dropify').dropify();
        }
      });

    $("#submit").click(function () {

    $.ajax({
        url: '<?= base_url('recooling/recooling/updaterecoolingWithDetail/').$id ?>',
        type: 'post',
        dataType: 'json',
        processData: false,                
        contentType: false,
        data: new FormData(frm),
        success: function (response) {
        
            Swal.fire({
                icon:   'success',
                title:  'Success!',
                html:   '<p class="h4">Your data is successfully input.</p>' 
                + '<a class="card-link" href="<?= base_url('recooling/recooling/list') ?>">Go to list page!</a>'
            });

        },
        error: function () {
            alert('gagal');
        }
    });
    });

</script>