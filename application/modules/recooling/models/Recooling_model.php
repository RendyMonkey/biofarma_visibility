<?php 
class recooling_model extends CI_Model{

	public function r_recooling_query($startDate = "", $endDate = ""){		
		$search_attribut = ["recooling.id_recooling", "recooling.category", "recooling.description", "recooling.foto", "recooling.time_recooling"," user_account.username"];
		$column_order = array(null, "recooling.id_recooling", "recooling.category", "recooling.description", "recooling.foto", "recooling.time_recooling"," user_account.username");
		$this->db->select("recooling.id_recooling, recooling.category, recooling.description, recooling.foto, recooling.time_recooling, user_account.username");
		$this->db->from('recooling');
		$this->db->join('user_account', 'user_account.id_user_account = recooling.id_user_account');
		if ($startDate != "" AND $endDate != "") {
			$this->db->where('recooling.created_date >=', $startDate);
			$this->db->where('recooling.created_date <=', $endDate);
		}else if($startDate != "" AND $endDate == ""){
			$this->db->where('recooling.created_date >=', $startDate);
		}else if($startDate == "" AND $endDate != ""){
			$this->db->where('recooling.created_date <=', $endDate);
		}
		$i = 0;	
	}

	public function r_recooling($startDate, $endDate){
		$this->r_recooling_query($startDate, $endDate);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	public function r_reccoling_report(){
		$this->r_recooling_query();
		$query = $this->db->get();
		return $query->result();
	}

	public function r_recooling_count_all()
    {
        $this->db->from('recooling');
        return $this->db->count_all_results();
	}
	
	function r_recooling_count_filtered($startDate, $endDate)
    {
        $this->r_recooling_query($startDate, $endDate);
        $query = $this->db->get();
        return $query->num_rows();
	}

	function r_recooling_by_id($id){
		$this->db->select("recooling.id_recooling, recooling.category, recooling.description, recooling.time_recooling, user_account.username");
		$this->db->from('recooling');
		$this->db->join('user_account', 'user_account.id_user_account = recooling.id_user_account');
		$query = $this->db->where('recooling.id_recooling', $id)->get()->row();
		return $query;
	}
}
?>