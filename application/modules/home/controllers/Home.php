<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Home extends CI_Controller {
	private $nama = "";

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('user/User_account_model'); 
 		$this->nama = $this->session->userdata()['nama'];
 		if(!$this->session->has_userdata('nama')){
 			redirect(base_url("login"));
 		}
 	}

 	public function index()
 	{
		if($this->session->userdata('nama_role') == "Administrator" || $this->session->userdata('nama_role') == "Distribusi"){
			$count_actived_user = $this->User_account_model->r_count_user_account_active();
			$count_all_user = $this->User_account_model->r_user_account_count_all();
			$count_login_user = $this->User_account_model->r_count_user_account_login();
		}else{
			$count_actived_user = $this->User_account_model->r_count_user_account_active_by_id_role_level_2($this->session->userdata("id_user_role_lv_2"));
			$count_all_user = $this->User_account_model->r_user_account_count_all_by_id_role_level_2($this->session->userdata("id_user_role_lv_2"));
			$count_login_user = $this->User_account_model->r_count_user_account_login_by_id_role_level_2($this->session->userdata("id_user_role_lv_2"));
		}
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('home_page', ['actived_user_count' => $count_actived_user, 'count_all_user' => $count_all_user, 'count_login_user' => $count_login_user]);
		$this->load->view('_rootComponents/_footer/footer');
 	}
}