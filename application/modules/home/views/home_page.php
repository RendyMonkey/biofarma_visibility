<link href="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css">
<link href="<?= base_url('assets/plugins/datatables/buttons.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css">
<link href="<?= base_url('assets/plugins/leaflet/leaflet.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.js') ?>"></script>
<div class="page-wrapper">
        <!-- Page Content-->
        <div class="page-content-tab">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">Dashboard</a></li>
                                </ol>
                            </div>
                            <h4 class="page-title">Dashboard</h4>
                        </div>
                        <!--end page-title-box-->
                    </div>
                    <!--end col-->
                </div><!-- end page title end breadcrumb -->
                <div class="row">
                    <!--end col-->
                    <div class="col-lg-3">
                    <div class="card">
                            <div class="card-body justify-content-center">
                                <div class="row">
                                    <div class="col-4 align-self-center text-center"><i data-feather="user-check"
                                            class="align-self-center icon-md icon-dual-success"></i></div>
                                    <!--end col-->
                                    <div class="col-8">
                                        <h3 class="mt-0 mb-1 font-weight-semibold"><?=$actived_user_count?></h3>
                                        <p class="mb-0 font-12 text-muted text-uppercase font-weight-semibold-alt">Actived User</p>
                                    </div>
                                    <!--end col-->
                                </div>
                                <!--end row-->
                            </div>
                    </div>
                    </div>
                    <div class="col-lg-3">
                    <div class="card">
                            <div class="card-body justify-content-center">
                                <div class="row">
                                    <div class="col-4 align-self-center text-center"><i data-feather="users"
                                            class="align-self-center icon-md icon-dual-warning"></i></div>
                                    <!--end col-->
                                    <div class="col-8">
                                        <h3 class="mt-0 mb-1 font-weight-semibold"><?=$count_all_user;?></h3>
                                        <p class="mb-0 font-12 text-muted text-uppercase font-weight-semibold-alt">Total User</p>
                                    </div>
                                    <!--end col-->
                                </div>
                                <!--end row-->
                            </div>
                    </div>
                    </div>
                    <div class="col-lg-3">
                    <div class="card">
                            <div class="card-body justify-content-center">
                                <div class="row">
                                    <div class="col-4 align-self-center text-center"><i data-feather="log-in"
                                            class="align-self-center icon-md icon-dual-primary"></i></div>
                                    <!--end col-->
                                    <div class="col-8">
                                        <h3 class="mt-0 mb-1 font-weight-semibold"><?=$count_login_user;?></h3>
                                        <p class="mb-0 font-12 text-muted text-uppercase font-weight-semibold-alt">User Login
                                        </p>
                                    </div>
                                    <!--end col-->
                                </div>
                                <!--end row-->
                            </div>
                    </div>
                    </div>
                    <div class="col-lg-3">
                    <div class="card">
                            <div class="card-body justify-content-center">
                                <div class="row">
                                    <div class="col-12 align-self-center text-center">
                                        <img src="<?= base_url('assets/images/logo-biofarma.png');?>" width="130" height="55">
                                    </div>
                                    <!--end col-->
                                </div>
                                <!--end row-->
                            </div>
                    </div>
                    </div>
            </div><!-- container -->


            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body"  class="chart-container" style="position: relative; width:80vw">
                            <h4 class="mt-0 header-title">Chart</h4>
                            <canvas id="myChart"></canvas>
                        </div>
                        <!--end card-body-->
                    </div>
                    <!--end card-->
                </div><!-- end col -->
            </div><!-- end row -->


            <?php if ($this->session->userdata('nama_role') == 'Administrator' || $this->session->userdata('nama_role') == 'Distribusi'){ ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Lokasi Kanal Distribusi</h4>
                            <div id="Leaf_default" class="" style="height: 400px"></div>
                        </div>
                        <!--end card-body-->
                    </div>
                    <!--end card-->
                </div><!-- end col -->
            </div><!-- end row -->
            <?php }?>


            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title mt-0 mb-4">Emergency</h4>
                            <table id="datatable" class="table table-bordered dt-responsive nowrap"
                                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Driver</th>
                                        <th>Category</th>
                                        <th>Time</th>
                                        <th>Description</th>
                                    </tr>
                                </thead>
                                <tbody style="text-align: center;">
                                </tbody>
                            </table>
                        </div>
                        <!--end card-body-->
                    </div>
                    <!--end card-->
                </div>
                <!--end col-->
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title mt-0 mb-4">Recolling</h4>
                            <table id="datatable2" class="table table-bordered dt-responsive nowrap"
                                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Driver</th>
                                        <th>Category</th>
                                        <th>Time</th>
                                        <th>Description</th>
                                    </tr>
                                </thead>
                                <tbody style="text-align: center;">
                                </tbody>
                            </table>
                        </div>
                        <!--end card-body-->
                    </div>
                    <!--end card-->
                </div>
                <!--end col-->
            </div>

            <!-- <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Lokasi Truck</h4>
                            <div id="lokasi_truck" class="" style="height: 400px"></div>
                        </div>
                        <!--end card-body-->
                    </div>
                    <!--end card-->
                </div><!-- end col -->
            </div> -->


            <!--  Modal content for the above example -->
            <div class="modal modal-rightbar fade" tabindex="-1" role="dialog" aria-labelledby="MetricaRightbar"
                aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title mt-0" id="MetricaRightbar">Appearance</h5><button type="button"
                                class="btn btn-sm btn-soft-primary btn-circle-sm btn-square" data-dismiss="modal"
                                aria-hidden="true"><i class="mdi mdi-close"></i></button>
                        </div>
                        <div class="modal-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-pills nav-justified mt-2 mb-4" role="tablist">
                                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#ActivityTab"
                                        role="tab">Activity</a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#TasksTab"
                                        role="tab">Tasks</a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#SettingsTab"
                                        role="tab">Settings</a></li>
                            </ul><!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="ActivityTab" role="tabpanel">
                                    <div class="bg-light mx-n3"><img src="<?= base_url('assets/images/small/img-1.gif') ?>" alt=""
                                            class="d-block mx-auto my-4" height="180"></div>
                                    <div class="slimscroll scroll-rightbar">
                                        <div class="activity">
                                            <div class="activity-info">
                                                <div class="icon-info-activity"><i
                                                        class="mdi mdi-checkbox-marked-circle-outline bg-soft-success"></i>
                                                </div>
                                                <div class="activity-info-text mb-2">
                                                    <div class="mb-1"><small class="text-muted d-block mb-1">10 Min
                                                            ago</small> <a href="#" class="m-0 w-75">Task finished</a>
                                                    </div>
                                                    <p class="text-muted mb-2 text-truncate">There are many variations
                                                        of passages.</p>
                                                </div>
                                            </div>
                                            <div class="activity-info">
                                                <div class="icon-info-activity"><i
                                                        class="mdi mdi-timer-off bg-soft-pink"></i></div>
                                                <div class="activity-info-text mb-2">
                                                    <div class="mb-1"><small class="text-muted d-block mb-1">50 Min
                                                            ago</small> <a href="#" class="m-0 w-75">Task Overdue</a>
                                                    </div>
                                                    <p class="text-muted mb-2 text-truncate">There are many variations
                                                        of passages.</p><span
                                                        class="badge badge-soft-secondary">Design</span> <span
                                                        class="badge badge-soft-secondary">HTML</span>
                                                </div>
                                            </div>
                                            <div class="activity-info">
                                                <div class="icon-info-activity"><i
                                                        class="mdi mdi-alert-decagram bg-soft-purple"></i></div>
                                                <div class="activity-info-text mb-2">
                                                    <div class="mb-1"><small class="text-muted d-block mb-1">10 hours
                                                            ago</small> <a href="#" class="m-0 w-75">New Task</a></div>
                                                    <p class="text-muted mb-2 text-truncate">There are many variations
                                                        of passages.</p>
                                                </div>
                                            </div>
                                            <div class="activity-info">
                                                <div class="icon-info-activity"><i
                                                        class="mdi mdi-clipboard-alert bg-soft-warning"></i></div>
                                                <div class="activity-info-text mb-2">
                                                    <div class="mb-1"><small
                                                            class="text-muted d-block mb-1">yesterday</small> <a
                                                            href="#" class="m-0 w-75">New Comment</a></div>
                                                    <p class="text-muted mb-2 text-truncate">There are many variations
                                                        of passages.</p>
                                                </div>
                                            </div>
                                            <div class="activity-info">
                                                <div class="icon-info-activity"><i
                                                        class="mdi mdi-clipboard-alert bg-soft-secondary"></i></div>
                                                <div class="activity-info-text mb-2">
                                                    <div class="mb-1"><small class="text-muted d-block mb-1">01 feb
                                                            2020</small> <a href="#" class="m-0 w-75">New Lead
                                                            Meting</a></div>
                                                    <p class="text-muted mb-2 text-truncate">There are many variations
                                                        of passages.</p>
                                                </div>
                                            </div>
                                            <div class="activity-info">
                                                <div class="icon-info-activity"><i
                                                        class="mdi mdi-checkbox-marked-circle-outline bg-soft-success"></i>
                                                </div>
                                                <div class="activity-info-text mb-2">
                                                    <div class="mb-1"><small class="text-muted d-block mb-1">26 jan
                                                            2020</small> <a href="#" class="m-0 w-75">Task finished</a>
                                                    </div>
                                                    <p class="text-muted mb-2 text-truncate">There are many variations
                                                        of passages.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end activity-->
                                    </div>
                                    <!--end activity-scroll-->
                                </div>
                                <div class="tab-pane" id="SettingsTab" role="tabpanel">
                                    <div class="p-1 bg-light mx-n3">
                                        <h6 class="px-3">Account Settings</h6>
                                    </div>
                                    <div class="p-2 text-left mt-3">
                                        <div class="custom-control custom-switch switch-primary mb-3"><input
                                                type="checkbox" class="custom-control-input" id="settings-switch1"
                                                checked=""> <label class="custom-control-label"
                                                for="settings-switch1">Auto updates</label></div>
                                        <div class="custom-control custom-switch switch-primary mb-3"><input
                                                type="checkbox" class="custom-control-input" id="settings-switch2">
                                            <label class="custom-control-label" for="settings-switch2">Location
                                                Permission</label></div>
                                        <div class="custom-control custom-switch switch-primary mb-3"><input
                                                type="checkbox" class="custom-control-input" id="settings-switch3"
                                                checked=""> <label class="custom-control-label"
                                                for="settings-switch3">Show offline Contacts</label></div>
                                    </div>
                                    <div class="p-1 bg-light mx-n3">
                                        <h6 class="px-3">General Settings</h6>
                                    </div>
                                    <div class="p-2 text-left mt-3">
                                        <div class="custom-control custom-switch switch-primary mb-3"><input
                                                type="checkbox" class="custom-control-input" id="settings-switch4"
                                                checked=""> <label class="custom-control-label"
                                                for="settings-switch4">Show me Online</label></div>
                                        <div class="custom-control custom-switch switch-primary mb-3"><input
                                                type="checkbox" class="custom-control-input" id="settings-switch5">
                                            <label class="custom-control-label" for="settings-switch5">Status visible to
                                                all</label></div>
                                        <div class="custom-control custom-switch switch-primary mb-3"><input
                                                type="checkbox" class="custom-control-input" id="settings-switch6"
                                                checked=""> <label class="custom-control-label"
                                                for="settings-switch6">Notifications Popup</label></div>
                                    </div>
                                </div>
                                <!--end tab-pane-->
                            </div>
                            <!--end tab-content-->
                        </div>
                        <!--end modal-body-->
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <footer class="footer text-center text-sm-left">&copy; 2020 Biotracking</footer>
            <!--end footer-->
        </div><!-- end page content -->
    </div><!-- end page-wrapper -->

<script src="<?= base_url('assets/plugins/leaflet/leaflet.js') ?>"></script>
<script src="<?= base_url('assets/plugins/leaflet/jquery.leaflet-map.init.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

<script>
    var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['January', 'Febuary', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
        datasets: [{
            label: "1",
            backgroundColor: 
                'red'
            ,
            data: [12, 19, 3, 5, 4, 3, 4, 8, 14, 10, 30, 5],
            // borderColor: [
            //     'rgba(255, 99, 132, 1)',
            // ],
            // borderWidth: 1
        },{
            label: "2",
            backgroundColor: 
                '#f1bd15'
            ,
            data: [11, 9, 4, 5, 7, 8, 5, 6, 10, 2, 20, 15],
            // borderColor: [
            //     'rgba(255, 99, 132, 1)',
            // ],
            // borderWidth: 1
        },{
            label: "2",
            backgroundColor: 
                '#2295af'
            ,
            data: [11, 15, 8, 3, 7, 2, 7, 7, 11, 4, 10, 6],
            // borderColor: [
            //     'rgba(255, 99, 132, 1)',
            // ],
            // borderWidth: 1
        }]
    },
    options: {
        legend: {
        display: false
        },
        scales: {
            xAxes: [{
                type: 'category',
                llabels: ['January', 'Febuary', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
            }]
        }
    }
});
</script>
</script>

<script>                                      

        $('#datatable').DataTable({ 
            "scrollX": true,
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?= site_url('Emergency/getEmergency2')?>",
                "type": "POST"
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                { 
                    "targets": [ 0 ], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });                     

        $('#datatable2').DataTable({ 
            "scrollX": true,
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?= site_url('recooling/getRecooling')?>",
                "type": "POST"
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                { 
                    "targets": [ 0 ], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

    });



        $.ajax({
            url: "<?= base_url('delivery_order/getLocationWarehouse') ?>",
            type: 'get',
            dataType: 'json',
            success: function(data) {
                for (var i = 0; i < data.length; i++) {
                    marker = new L.marker([data[i][1],data[i][2]])
                    .bindPopup(data[i][0])
                    .addTo(map);
                }             
            },
            error: function () {
              alert('gagal');
            }
          });


    var map = L.map('Leaf_default').setView([-6.9024759, 107.6166213], 8);
    mapLink = 
        '<a href="http://openstreetmap.org">OpenStreetMap</a>';
    L.tileLayer(
        'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; ' + mapLink + ' Contributors',
        maxZoom: 18,
        }).addTo(map);
</script>