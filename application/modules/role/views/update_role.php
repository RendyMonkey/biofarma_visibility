<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= base_url() ?>">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">Role</a></li>
                                    <li class="breadcrumb-item active">Create Role</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Update Role</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                        <form class="needs-validation" novalidate>
                            <div class="form-row">
                              <div class="col-md-6 mb-3">
                                <label for="validationCustom02">Code Role</label>
                                <input type="text" class="form-control" id="kd_role" required value="<?= $data->kd_role ?>">
                                <div class="invalid-feedback">
                                  This field is required!
                                </div>
                              </div>

                              <div class="col-md-6 mb-3">
                                <label for="validationCustom02">Role Name</label>
                                <input type="text" class="form-control" id="nm_role" required value="<?= $data->nm_role ?>">
                                <div class="invalid-feedback">
                                  This field is required!
                                </div>
                              </div>
                            </div>
                            <button class="btn btn-primary" type="button" id="submit">Submit form</button>
                          </form>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container -->
    </div><!-- end page content -->
</div><!-- end page-wrapper -->

<script>


$('#submit').click( function(){
    if(($('#kd_role').val().length < 1) || ($('#nm_role').val().length < 1)){
          if($('#kd_role').val().length < 1){
            document.getElementById('kd_role').focus();
            var message = "Kode Role is required";
          }else if($('#nm_role').val().length < 1){
            document.getElementById('nm_role').focus();
            var message = "Name Role is required";
          }
          Swal.fire(
              'Input Warning !',
              message,
              'warning'
          )
        }else{
        $.ajax({
            url: '<?= base_url('role/role/updateRole/').$data->id_role ?>',
            type: 'post',
            dataType: 'json',
            data: {
                <?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>',
                kd_role: $('#kd_role').val(),
                nm_role: $('#nm_role').val()
            },
            success: function (data) {
                Swal.fire(
                'Data has been created!',
                'Your data is successfuly created.',
                'success'
                );
                
                $('#nama_menu').val('');
                $('#category').val('');
            }, error: function(){
                Swal.fire(
                'Data not created!',
                'Please contact developer to fix it.',
                'error'
                )
            }
        })
    }
       });
</script>