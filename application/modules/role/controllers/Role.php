<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Role extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 		$this->load->model('Role_model');
 		$this->load->model('User/User_role_level_2_model');
 		if(!$this->session->has_userdata('nama')){
 			redirect(base_url("login"));
 		}
 	}

 	public function createRole()
 	{
 		$data = [
 			"kd_role" => $this->input->post('kd_role'),
 			"nm_role" => $this->input->post('nm_role')
 		];
 		$input = $this->Role_model->c_role($this->security->xss_clean($data));
 		if($input){
 			log_record(null, json_encode($data));
 			echo json_encode(['message' => 'input data berhasil']);
 		}else{
 			echo json_encode(['message' => 'input data gagal']);
 		}
	 }
	 
 	public function getRole()
 	{
		$list = $this->Role_model->r_role();
		$all_data = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$row = array();
			$no++;
			$row[] = $no;
			$row[] = $data->kd_role;
			$row[] = $data->nm_role;
			$row[] = '<a href="'. base_url("role/update/").$data->id_role.'" class="mr-2">
					   	<i class="las la-pen text-info font-18"></i>
					   </a> 
                    <a href="#" onclick="delete_data('.$data->id_role.')">
					 <i class="las la-trash-alt text-danger font-18"></i>
												</a>';

			$all_data[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->Role_model->r_role_count_all(),
                        "recordsFiltered" => $this->Role_model->r_role_count_filtered(),
                        "data" => $all_data,
		); 
 		echo json_encode($result);
	 }
	 
	 public function getRoleAll() {
		$data = $this->Role_model->r_role_all();
		echo json_encode($data);
	 }


 	public function updateRole($id)
 	{
 		$data = [
 			"kd_role" => $this->input->post('kd_role'),
 			"nm_role" => $this->input->post('nm_role')
 		];

 		log_record(json_encode($this->Role_model->r_role_by_id($id)), json_encode($data));
 		$update = $this->Role_model->u_role($id, $this->security->xss_clean($data));
 		if($update){
 			echo json_encode(['message' => 'update data berhasil']);
 		}else{
 			echo json_encode(['message' => 'update data gagal']);
 		}
 	}

 	public function deleteRole($id)
 	{
 		log_record(json_encode($this->Role_model->r_role_by_id($id)), null);
 		$delete = $this->Role_model->d_role($id);
 		echo json_encode(['message' => 'delete data berhasil']);
	 }

	public function list()
	{
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('list_role');
		$this->load->view('_rootComponents/_footer/footer');
	}

	public function create()
	{
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('create_role');
		$this->load->view('_rootComponents/_footer/footer');
	}

	public function update($id)
	{
		$data = $this->Role_model->r_role_by_id($id);
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('update_role', ['data' => $data]);
		$this->load->view('_rootComponents/_footer/footer');
	}

	public function list_lvl_2()
	{
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('role_lvl_2/list_role');
		$this->load->view('_rootComponents/_footer/footer');
	}

	public function create_lvl_2()
	{
		$this->load->view('_rootComponents/_header/heder');

		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('role_lvl_2/create_role');
		$this->load->view('_rootComponents/_footer/footer');
	}

	public function update_lvl_2($id)
	{
		$data = $this->User_role_level_2_model->r_user_role_level_2_by_id($id);
		// echo json_encode($data);
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('role_lvl_2/update_role', ['data' => $data]);
		$this->load->view('_rootComponents/_footer/footer');
	}

	public function getAllRoleWhereNotById($id){
		echo json_encode($this->Role_model->r_role_for_where_not_byId($id));
	}
}