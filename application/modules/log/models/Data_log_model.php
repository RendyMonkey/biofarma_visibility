<?php 
class Data_log_model extends CI_Model{
	public function c_log($data){
		$query = $this->db->insert('data_log', $data);
		return $query;
	}

	public function r_log_query($startDate = "", $endDate = ""){		
		$search_attribut = ["user_account.username", "data_log.new_data", "data_log.old_data", "data_log.source"];
		$column_order = array(null, "user_account.username", "data_log.new_data", "data_log.old_data", "data_log.source");
		$this->db->select("user_account.username, data_log.new_data, data_log.old_data, data_log.source");
		$this->db->from('data_log');
		$this->db->join('user_account', 'user_account.id_user_account = data_log.user_id');
		if ($startDate != "" AND $endDate != "") {
			$this->db->where('data_log.created_date >=', $startDate);
			$this->db->where('data_log.created_date <=', $endDate);
		}else if($startDate != "" AND $endDate == ""){
			$this->db->where('data_log.created_date >=', $startDate);
		}else if($startDate == "" AND $endDate != ""){
			$this->db->where('data_log.created_date <=', $endDate);
		}
		$i = 0;	
	}

	public function r_log($startDate, $endDate){
		$this->r_log_query($startDate, $endDate);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	public function tests(){
		$this->r_log_query();
		$query = $this->db->get();
		return $query->result();
	}

	public function r_log_report(){
		$this->r_log_query();
		$query = $this->db->get();
		return $query->result();
	}

	public function r_log_count_all()
    {
        $this->db->from('data_log');
        return $this->db->count_all_results();
	}
	
	function r_log_count_filtered($startDate, $endDate)
    {
        $this->r_log_query($startDate, $endDate);
        $query = $this->db->get();
        return $query->num_rows();
	}
}
?>