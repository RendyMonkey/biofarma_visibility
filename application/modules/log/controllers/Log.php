<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Log extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->library('session');
 		$this->load->helper('url');
 		$this->load->model('Data_log_model');
 		if(!$this->session->has_userdata('nama')){
 			redirect(base_url("login"));
 		}
 	}

    public function getLog()
 	{
		$startDate = $this->input->post('startDate');
		$endDate = $this->input->post('endDate');
		$list = $this->Data_log_model->r_log($startDate, $endDate);
		$all_data = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$row = array();
			$no++;
			$row[] = $no;
			$row[] = $data->username;
			$row[] = $data->new_data;
			$row[] = $data->old_data;
			$row[] = $data->source;

			$all_data[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->Data_log_model->r_log_count_all(),
                        "recordsFiltered" => $this->Data_log_model->r_log_count_filtered($startDate, $endDate),
                        "data" => $all_data,
		); 
 		echo json_encode($result);
 	}

 	public function test(){
 		echo json_encode($this->Data_log_model->tests());
 	}

    public function log_admin()
	{
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('list_log_admin');
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function log_visibility()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('list_log_visibility');
		$this->load->view('_rootComponents/_footer/footer');
	 }
 
}