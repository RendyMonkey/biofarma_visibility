<?php 
class Receive_order_model extends CI_Model{

	public function r_receive_order_query(){
		$search_attribut = ["delivery_order.no_do", "delivery_order.tanggal_pengiriman", "delivery_order.time_finish_receive", "delivery_order.status", "user_role_level_2.nm_role_lvl_2"];
		$column_order = array(null, "delivery_order.no_do", "delivery_order.tanggal_pengiriman", "delivery_order.time_finish_receive", "delivery_order.status", "user_role_level_2.nm_role_lvl_2");
		$this->db->select("delivery_order.no_do, delivery_order.tanggal_pengiriman, delivery_order.time_finish_receive, delivery_order.status, user_role_level_2.nm_role_lvl_2");
		$this->db->from('delivery_order');
		$this->db->join('user_role_level_2', 'user_role_level_2.id_user_role = delivery_order.id_role_receive');
		$this->db->where('delivery_order.status', 3);
		$this->db->where('delivery_order.id_role_receive', $this->session->userdata('id_user_role_lv_2'));
		$i = 0;
	}

	public function r_receive_order() 
	{
		$this->r_receive_order_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}

	public function r_receive_order_report(){
		$this->r_receive_order_query();
		return $this->db->get()->result();
	}

	function r_receive_order_count_filtered()
    {
        $this->r_receive_order_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
	
    public function r_receive_order_count_all()
    {
        $this->db->from('delivery_order');
        $this->db->where('status', 3);
        return $this->db->count_all_results();
	}

	public function r_receive_order_by_no_do($id){
		$this->db->select("delivery_order.no_do, delivery_order.tanggal_pengiriman, delivery_order.time_finish_receive, delivery_order.status, user_role_level_2.nm_role_lvl_2");
		$this->db->from('delivery_order');
		$this->db->join('user_role_level_2', 'user_role_level_2.id_user_role = delivery_order.id_role_receive');
		$this->db->where('delivery_order.no_do', $id);
		return $this->db->get()->row();
	}

	public function u_detail_delivery_order($gsOneId){
		$data = array(
        	'status_received' => 1,
		);
		$this->db->where('gs_on_id', $gsOneId);
		return $this->db->update('detail_delivery_order', $data);
	}
}
?>