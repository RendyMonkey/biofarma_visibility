<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Receive_order extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 		$this->load->model('Receive_order_model');
 		if(!$this->session->has_userdata('nama')){
 			redirect(base_url("login"));
 		}
 	}

 	public function createReceiveOrder(){
 		foreach ($this->input->post('gsOneId') as $data) {
 			$input_data = $this->Receive_order_model->u_detail_delivery_order($data['gsOneId']);
 		}
 		if($input_data){
 			echo json_encode(['message' => 'input data berhasil']);
 		}else{
 			echo json_encode(['message' => 'input data gagal']);
 		}
 	}

 	public function getReceiveOrder()
	 { 
		$list = $this->Receive_order_model->r_receive_order();
		$datas = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $data->no_do;
			$row[] = $data->nm_role_lvl_2;
			$row[] = $data->tanggal_pengiriman;
			$row[] = $data->time_finish_receive;
			if($data->status == 1){
				$row[] = "Dalam pengiriman";
			}else if($data->status == 2){
				$row[] = "Sedang dalam penerimaan";
			}else{
				$row[] = "Sudah diterima";
			}
			$row[] = '
					   <a href="'.base_url('receive_order/detail/').$data->no_do.'" class="mr-2">
					   	<i class="las la-clipboard text-info font-18"></i>
					   </a>';

			$datas[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->Receive_order_model->r_receive_order_count_all(),
                        "recordsFiltered" => $this->Receive_order_model->r_receive_order_count_filtered(),
                        "data" => $datas,
		); 
 		echo json_encode($result);
	 }

	public function getReceiveOrderByNoDo($no_do){
		echo json_encode($this->Receive_order_model->r_receive_order_by_no_do($no_do));
	}

	public function exportReportReceive(){
		$data = $this->Receive_order_model->r_receive_order_report();
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'No');
		$sheet->setCellValue('B1', 'No Delivery');
		$sheet->setCellValue('C1', 'Supplier');
		$sheet->setCellValue('D1', 'Date Delivery');
		$sheet->setCellValue('F1', 'Date Receive');
		$sheet->setCellValue('G1', 'Status');
		$no = 1;
		$x = 2;
		foreach($data as $row)
		{
			$sheet->setCellValue('A'.$x, $no++);
			$sheet->setCellValue('B'.$x, $row->no_do);
			$sheet->setCellValue('C'.$x, $row->nm_role_lvl_2);
			$sheet->setCellValue('D'.$x, $row->tanggal_pengiriman);
			$sheet->setCellValue('E'.$x, $row->time_finish_receive);
			if($row->status == 1){
				$sheet->setCellValue('G'.$x, "Dalam pengiriman");
			}else if($row->status == 2){
				$sheet->setCellValue('G'.$x, "Sedang dalam penerimaan");
			}else{
				$sheet->setCellValue('G'.$x, "Sudah diterima");
			}
			$x++;
		}
		$writer = new Xlsx($spreadsheet);
		$filename = 'Report_receive';
			
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');
	
		$writer->save('php://output');
	}
    public function list()
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('receive');
	   $this->load->view('_rootComponents/_footer/footer');
	}

	public function detail($id)
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('receive_detail', ['id'=>$id]);
	   $this->load->view('_rootComponents/_footer/footer');
	}
}