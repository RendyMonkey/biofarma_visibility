<?php 
	function log_record($old_data, $new_data)
	{
		$ci = & get_instance();
		$ci->load->library('session');
		$data = [
			"source" => "Web Visibility",
			"old_data" => $old_data,
			"new_data" => $new_data,
			"created_by" => $ci->session->userdata('id_user_account'),
			"created_date" => date("Y-m-d H:i:s"),			   
			"user_id" => $ci->session->userdata('id_user_account'),
 		];
		$create_log = $ci->db->insert('data_log', $data);
		return $create_log;
	}
 ?>